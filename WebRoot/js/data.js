ViewParser.ready(function(argument) {
    function initCallback(callback) {
        callback || (callback = {});
        callback.success || (callback.success = jQuery.noop);
        callback.error || (callback.error = jQuery.noop);
        callback.complete || (callback.complete = jQuery.noop);
        return callback
    }

    function ajax(ajaxConfig, callback) {
        var baseUrl = AjaxData.get("baseUrl");
        callback = initCallback(callback);
        ajaxConfig.url = baseUrl + ajaxConfig.url;
        var _success = callback.success;
        ajaxConfig.success = function(data) {
            if (data instanceof Array) {
                jQuery.each(data, function(index, dataItem) {
                    for (var i in dataItem) {
                        if (dataItem[i] && dataItem[i].time) {
                            dataItem[i] = new Date(dataItem[i].time + 28800000);
                        }
                    }
                })
            } else {
                for (var i in data) {
                    if (data[i] && data[i].time) {
                        data[i] = new Date(data[i].time + 28800000);
                    }
                }
            }
            _success.apply(this, arguments);
        }
        ajaxConfig.error = callback.error;
        ajaxConfig.complete = callback.complete;
        //日期格式统一使用毫秒数
        var dataString = Date.prototype.toString;
        Date.prototype.toString = function() {
            return +this
        };
        for (var i in ajaxConfig.data) {
            if (ajaxConfig.data[i]) {
                ajaxConfig.data[i] = ajaxConfig.data[i].valueOf().toString();
            }
        }
        //Ajax访问服务器
        var result = jQuery.ajax(ajaxConfig);
        //回复原生格式
        Date.prototype.toString = dataString;
        return result;
    }
    var AjaxData = window.AjaxData = DataManager({
        baseUrl: "http://localhost:8080/HouseSales/ajax",
        apartmentLayout: {
            add: function(data, callback) {
                var url = "/ApartmentLayout";
                data.method = "insert";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            delete:function(data,callback){
                var url = "/ApartmentLayout";
                data.method="delete";
                return ajax({
                    url:url,
                    data:data,
                    type:"POST"
                },callback);
            },
            update: function(data, callback) {
                var url = "/ApartmentLayout";
                data.method = "update";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            list: function(data, callback) {
                var url = "/ApartmentLayout";
                data || (data = {});
                callback || (callback = {
                    success: jQuery.noop
                });
                _success = callback.success;
                callback.success = function(data) {
                    _success && _success.apply(this, arguments);
                    App.set("Data.apartmentLayout", data);
                    App.set("Bak.apartmentLayout",data);
                }
                data.method = "findAll";
                return ajax({
                    url: url,
                    dataType: "json",
                    data: data
                }, callback);
            },
            findById: function(data, callback) {
                var url = "/ApartmentLayout";
                data.method = "findById";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            }
        },
        userBase: {
            getById: function(data, callback) {
                var baseUrl = AjaxData.get("baseUrl");
                var url = baseUrl + "/user";
                data.method = "getById";
                callback = initCallback(callback);

                return jQuery.ajax({
                    url: url,
                    data: data,
                    dataType: "text",
                    // dataType: "json",//最好不与dataFiter混合在一起写，用dataFilter手动解析JSON
                    dataFilter: function(data, dataType) {
                        try {
                            var result = jQuery.parseJSON(data);
                            var ubBirthDay = result.ubBirthDay;
                            result.ubBirthDay = tool.formatDate(ubBirthDay);
                            console.log(result);
                        } catch (e) {
                            result = jQuery.trim(data);
                        }
                        return result;
                    },
                    success: callback.success,
                    error: callback.error,
                    complete: callback.complete
                });
            }
        },
        customer: {
            add: function(data, callback) {
                var url = "/Customer";
                data.method = "insert";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback)
            },
            list: function(data, callback) {
                var url = "/Customer";
                data || (data = {});
                callback || (callback = {
                    success: jQuery.noop
                });
                _success = callback.success;
                callback.success = function(data) {
                    _success && _success.apply(this, arguments);
                    App.set("Data.customer", data);
                    App.set("Bak.customer",data);
                }
                data.method = "findAll";
                return ajax({
                    url: url,
                    dataType: "json",
                    data: data
                }, callback);
            },
            findById: function(data, callback) {
                var url = "/Customer";
                data.method = "findById";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            delete:function(data,callback){
                var url = "/Customer";
                data.method="delete";
                return ajax({
                    url:url,
                    data:data,
                    type:"POST"
                },callback);
            },
            update: function(data, callback) {
                var url = "/Customer";
                data.method = "update";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            }
        },
        salesStaff: {
            add: function(data, callback) {
                var url = "/SalesStaff";
                data.method = "insert";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback)
            },
            list: function(data, callback) {
                var url = "/SalesStaff";
                data || (data = {});
                callback || (callback = {
                    success: jQuery.noop
                });
                _success = callback.success;
                callback.success = function(data) {
                    _success && _success.apply(this, arguments);
                    App.set("Data.salesStaff", data);
                    App.set("Bak.salesStaff",data);
                }
                data.method = "findAll";
                return ajax({
                    url: url,
                    dataType: "json",
                    data: data
                }, callback);
            },
            findById: function(data, callback) {
                var url = "/SalesStaff";
                data.method = "findById";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            delete:function(data,callback){
                var url = "/SalesStaff";
                data.method="delete";
                return ajax({
                    url:url,
                    data:data,
                    type:"POST"
                },callback);
            },
            update: function(data, callback) {
                var url = "/SalesStaff";
                data.method = "update";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            }
        },
        estate: {
            add: function(data, callback) {
                var url = "/Estate";
                data.method = "insert";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            list: function(data, callback) {
                var url = "/Estate";
                data || (data = {});
                callback || (callback = {
                    success: jQuery.noop
                });
                _success = callback.success;
                callback.success = function(data) {
                    _success && _success.apply(this, arguments);
                    App.set("Data.estate", data);
                    App.set("Bak.estate",data);
                }
                data.method = "findAll";
                return ajax({
                    url: url,
                    dataType: "json",
                    data: data
                }, callback);
            },
            findById: function(data, callback) {
                var url = "/Estate";
                data.method = "findById";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            delete:function(data,callback){
                var url = "/Estate";
                data.method="delete";
                return ajax({
                    url:url,
                    data:data,
                    type:"POST"
                },callback);
            },
            update: function(data, callback) {
                var url = "/Estate";
                data.method = "update";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            }
        },
        building: {
            add: function(data, callback) {
                var url = "/Building";
                data.method = "insert";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            list: function(data, callback) {
                var url = "/Building";
                data || (data = {});
                callback || (callback = {
                    success: jQuery.noop
                });
                _success = callback.success;
                callback.success = function(data) {
                    _success && _success.apply(this, arguments);
                    App.set("Data.building", data);
                    App.set("Bak.building",data);
                }
                data.method = "findAll";
                return ajax({
                    url: url,
                    dataType: "json",
                    data: data
                }, callback);
            },
            findById: function(data, callback) {
                var url = "/Building";
                data.method = "findById";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            delete:function(data,callback){
                var url = "/Building";
                data.method="delete";
                return ajax({
                    url:url,
                    data:data,
                    type:"POST"
                },callback);
            },
            update: function(data, callback) {
                var url = "/Building";
                data.method = "update";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            }
        },
        contract: {
            add: function(data, callback) {
                var url = "/Contract";
                data.method = "insert";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            list: function(data, callback) {
                var url = "/Contract";
                data || (data = {});
                callback || (callback = {
                    success: jQuery.noop
                });
                _success = callback.success;
                callback.success = function(data) {
                    _success && _success.apply(this, arguments);
                    App.set("Data.contract", data);
                    App.set("Bak.contract",data);
                }
                data.method = "findAll";
                return ajax({
                    url: url,
                    dataType: "json",
                    data: data
                }, callback);
            },
            findById: function(data, callback) {
                var url = "/Contract";
                data.method = "findById";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            },
            delete:function(data,callback){
                var url = "/Contract";
                data.method="delete";
                return ajax({
                    url:url,
                    data:data,
                    type:"POST"
                },callback);
            },
            update: function(data, callback) {
                var url = "/Contract";
                data.method = "update";
                return ajax({
                    url: url,
                    type: "POST",
                    data: data
                }, callback);
            }
        }
    })
})
