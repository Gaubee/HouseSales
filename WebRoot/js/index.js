Date.prototype.toString = Date.prototype.toLocaleString;
ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = ["hint", "dropdown", "touch-handler", "dialog"];
    var mutualInit = {
        hint: function() {
            jQuery("[data-hint]").hint();
        },
        dropdown: function() {
            jQuery("[data-role='dropdown'").dropdown();
        },
        "touch-handler": false,
        dialog: false //async false，使用同步
    }


    //初始化MVVM应用程序
    ViewParser.app({
        Id: "navIndex",
        Data: {
            appInfo: {
                name: "楼房销售管理系统",
                nav: [{
                    title: "房产管理",
                    url: "HouseManage",
                    icon: "home",
                    childs: [{
                        title: "楼盘管理",
                        url: "Estate"
                    }, {
                        title: "户型管理",
                        url: "ApartmentLayout"
                    }, {
                        title: "楼房管理",
                        url: "Building"
                    }/*, {
                        title: "售楼统计",
                        url: "Statistics"
                    }*/]
                }, {
                    title: "客户管理",
                    url: "CustomerManage",
                    icon: "user"
                }, {
                    title: "合同管理",
                    url: "ContractManage",
                    icon: "libreoffice"
                }, {
                    title: "销售人员管理",
                    url: "SalesStaffManage",
                    icon: "user-3"
                }]
            },
            Data: {
                /*用于保存所有列表数据*/
                customer: [],
                salesStaff: [],
                contract: [],
                apartmentLayout: [],
                estate: [],
                building: []
            },
            apartmentLayoutSearch: function(e, vi) {
                var apartmentLayout = App.get("Bak.apartmentLayout");
                var value = jQuery.trim(this.value);
                if (value) {
                    console.log(value,apartmentLayout)
                    var result = [];
                    jQuery.each(apartmentLayout,function(index,apartmentLayoutItem){
                        for(var i in apartmentLayoutItem){
                            if (String(apartmentLayoutItem[i]).indexOf(value)!==-1) {
                                result.push(apartmentLayoutItem);
                                break;
                            }
                        }
                    })
                    App.set("Data.apartmentLayout",result);
                }else{
                    App.set("Data.apartmentLayout",apartmentLayout);
                }
            },
            buildingSearch: function(e, vi) {
                var building = App.get("Bak.building");
                var value = jQuery.trim(this.value);
                if (value) {
                    console.log(value,building)
                    var result = [];
                    jQuery.each(building,function(index,buildingItem){
                        for(var i in buildingItem){
                            if (String(buildingItem[i]).indexOf(value)!==-1) {
                                result.push(buildingItem);
                                break;
                            }
                        }
                    })
                    App.set("Data.building",result);
                }else{
                    App.set("Data.building",building);
                }
            },
            contractSearch: function(e, vi) {
                var contract = App.get("Bak.contract");
                var value = jQuery.trim(this.value);
                if (value) {
                    console.log(value,contract)
                    var result = [];
                    jQuery.each(contract,function(index,contractItem){
                        for(var i in contractItem){
                            if (String(contractItem[i]).indexOf(value)!==-1) {
                                result.push(contractItem);
                                break;
                            }
                        }
                    })
                    App.set("Data.contract",result);
                }else{
                    App.set("Data.contract",contract);
                }
            },
            customerSearch: function(e, vi) {
                var customer = App.get("Bak.customer");
                var value = jQuery.trim(this.value);
                if (value) {
                    console.log(value,customer)
                    var result = [];
                    jQuery.each(customer,function(index,customerItem){
                        for(var i in customerItem){
                            if (String(customerItem[i]).indexOf(value)!==-1) {
                                result.push(customerItem);
                                break;
                            }
                        }
                    })
                    App.set("Data.customer",result);
                }else{
                    App.set("Data.customer",customer);
                }
            },
            estateSearch: function(e, vi) {
                var estate = App.get("Bak.estate");
                var value = jQuery.trim(this.value);
                if (value) {
                    console.log(value,estate)
                    var result = [];
                    jQuery.each(estate,function(index,estateItem){
                        for(var i in estateItem){
                            if (String(estateItem[i]).indexOf(value)!==-1) {
                                result.push(estateItem);
                                break;
                            }
                        }
                    })
                    App.set("Data.estate",result);
                }else{
                    App.set("Data.estate",estate);
                }
            },
            salesStaffSearch: function(e, vi) {
                var salesStaff = App.get("Bak.salesStaff");
                var value = jQuery.trim(this.value);
                if (value) {
                    console.log(value,salesStaff)
                    var result = [];
                    jQuery.each(salesStaff,function(index,salesStaffItem){
                        for(var i in salesStaffItem){
                            if (String(salesStaffItem[i]).indexOf(value)!==-1) {
                                result.push(salesStaffItem);
                                break;
                            }
                        }
                    })
                    App.set("Data.salesStaff",result);
                }else{
                    App.set("Data.salesStaff",salesStaff);
                }
            }
        }
    });

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)

});
