(function() {
    var mutualCaches = {};
    var tool = window.tool = {
        formatDate: function(json) {
            var result = new Date();

            result.setHours(json.hours);
            result.setMinutes(json.minutes);
            result.setSeconds(json.seconds);
            result.setYear(json.year);
            result.setMonth(json.month);
            result.setDate(json.date);
            return result;
        },
        requireMutual: function(mutualScripts, mutualInit) {
            mutualInit || (mutualInit = {});
            jQuery.each(mutualScripts, function(index, mutualName) {
                var url = "js/vendor/metro/metro-" + mutualName + ".js";
                console.log(index, mutualName, url);

                var cache = mutualCaches[url];
                if (cache) {
                    (mutualInit[mutualName] || jQuery.noop).apply(cache.t, cache.args);
                } else {
                    var async = mutualInit[mutualName] === false ? false : true;
                    jQuery.ajax({
                        url: url,
                        dataType: "text",
                        async: async,
                        cache: true,
                        success: function(scriptText) {
                            Function(scriptText)();
                            (mutualInit[mutualName] || jQuery.noop).apply(this, arguments);
                            mutualCaches[url] = {
                                t: this,
                                args: arguments
                            };
                        }
                    });
                }
            });
        }
    }
}());
