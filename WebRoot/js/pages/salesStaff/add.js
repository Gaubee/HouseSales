ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = ["core", "locale", "calendar", "datepicker" /*,"dialog"*/ , "progressbar", "notify", "input-control"];
    var mutualInit = {
        core: false,
        global: false,
        locale: false,
        calendar: false,
        datepicker: function() {
            console.log("init datepicker");
            $("[data-role='datepicker']").datepicker({
                selected: function(dataStr, dataInstance) {
                    var datepicker = this;
                    var input = datepicker.find("input[type=text]");
                    var bind_key = input.attr("bind-form-key");
                    salesStaffAdd.set(bind_key, dataInstance)
                }
            });
        },
        "input-control":function(){
            jQuery(".input-control").inputControl();
        }
    };

    var salesStaffAdd = ViewParser.build({
        Url: "pages/salesStaff/add.html",
        Var: "salesStaffAdd",
        Data: {
            TYPE:"添加",
            Forms: [{
                label: "身份证号码",
                name: "ubId", //khUbId
                type: "text"
            }, {
                label: "姓名",
                name: "ubName",
                type: "text"
            }, {
                label: "性别",
                name: "ubSex",
                type: "radio",
                radio: ["男", "女"]
            }, {
                label: "电话号码",
                name: "ubTel",
                type: "text"
            }, {
                label: "出生年月",
                name: "ubBirthDay",
                type: "date"
            }, {
                label: "户口地址",
                name: "ubAddress",
                type: "text"
            }, {
                label: "电子邮箱",
                name: "ubEmail",
                type: "text"
            }, {
                label: "社交网络",
                name: "ubSns",
                type: "text"
            }, {
                label: "备注",
                name: "ubRemark",
                type: "textArea"
            }],
            Event: {
                submit: function(e, vi) {
                    var dialogContent = jQuery("salesStaffAddSubmit").html();
                    $.Dialog({
                        overlay: true,
                        shadow: true,
                        draggable: true,
                        icon: '<span class="icon-box-add"></span>',
                        title: '确定添加？',
                        width: 320,
                        padding: 10,
                        content: dialogContent,
                        onShow: function(dialog) {
                            dialog.on("click", ".yes", function() {
                                // Ajax..get("salesStaff.add")(salesStaffAdd.get("Form"))
                                //     .done(function() {
                                //         dialog.find(".btn-close").click();
                                //     });
                                var form_data = salesStaffAdd.get("Forms");
                                var format_data = {};
                                var stopAniamate = false;
                                jQuery.each(form_data, function(index, data) {
                                    format_data[data.name] = data.value;
                                });
                                AjaxData.get("salesStaff.add")(format_data, {
                                    success: function(ajaxResult) {
                                        stopAniamate = "success";
                                    },
                                    error: function(ajaxResult) {
                                        stopAniamate = "error"
                                    }
                                });
                                //上传数据的动画
                                var pb = dialog.find("[data-role='progress-bar']").progressbar();
                                var value = 0;
                                var ti = setInterval(function() {
                                    if (stopAniamate = "success") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "添加销售人员信息成功",
                                            style: {
                                                background: "green",
                                                color: "black"
                                            }
                                        });
                                    } else if (stopAniamate = "error") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "添加销售人员信息失败",
                                            style: {
                                                background: "darkRed",
                                                color: "white"
                                            }
                                        });
                                    }

                                    try {
                                        value++;
                                        pb.progressbar("value", value % 100);
                                    } catch (e) {
                                        clearInterval(ti);
                                    }
                                }, 20)
                            }).on("click", ".no", function() {
                                dialog.find(".btn-close").click();
                            })
                        }
                    });
                }
            }
        },
    });
    App.set("VIS.salesStaff.add", salesStaffAdd);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
