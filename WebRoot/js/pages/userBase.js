ViewParser.ready(function() {
    console.log("loadded userBaseManage");
    //交互所需的脚本
    var mutualScripts = ["notify"];
    //初始化应用程序MVVM框架
    var userBaseManageApp = ViewParser.build({
        Url: "pages/userBase.html",
        Var: "userBaseManageApp",
        Data: {
            //用于保存页面临时数据
            session: {},
            //用于存储事件
            Event: {
                //搜索
                search: function(e, vi) {
                    AjaxData.get("userBase.getById")({
                        id: userBaseManageApp.get("session.search")
                    }, {
                        success: function(data) {
                            console.log("data:", data, data == "null")
                            if (data != "null") {
                                if (!(data instanceof Array)) {
                                    data = [data];
                                }
                                userBaseManageApp.set("session.searchResult", data);
                                userBaseManageApp.set("Mutual.searchResponse", "success-state");
                            } else {
                                userBaseManageApp.set("session.searchResult", null);
                                $.Notify({
                                    content: "找不到相关数据！",
                                    style: {
                                        background: "orange",
                                        color: "white"
                                    }
                                });
                                userBaseManageApp.set("Mutual.searchResponse", "warning-state");
                            }
                        },
                        error: function(errorInfo) {
                            $.Notify({
                                content: "查询失败，请检查连接！(" + errorInfo + ")",
                                style: {
                                    background: "darkRed",
                                    color: "white"
                                }
                            });
                            userBaseManageApp.set("Mutual.searchResponse", "error-state");
                        }
                    })
                }
            },
            //交互
            Mutual: {
                searchResponse: "",
                searchInput: function(e, vi) {
                    userBaseManageApp.set("Mutual.searchResponse", "");
                }
            }
        }
    });

    //加载交互所需的脚本
    jQuery.each(mutualScripts, function(index, mutualName) {
        console.log("loadding... ", index, mutualName)
        jQuery.getScript("js/vendor/metro/metro-" + mutualName + ".js");
    });
});
