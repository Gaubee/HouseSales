ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = ["core", "locale", "calendar", "datepicker" /*,"dialog"*/ , "progressbar", "notify", "input-control"];
    var mutualInit = {
        core: false,
        global: false,
        locale: false,
        calendar: false,
        datepicker: function() {
            console.log("init datepicker");
            $("[data-role='datepicker']").datepicker({
                selected: function(dataStr, dataInstance) {
                    var datepicker = this;
                    var input = datepicker.find("input[type=text]");
                    var bind_key = input.attr("bind-form-key");
                    buildingUpdate.set(bind_key, dataInstance)
                }
            });
        },
        "input-control": function() {
            jQuery(".input-control").inputControl();
        }
    };

    if (App.get("Data.apartmentLayout").length === 0) {
        AjaxData.get("apartmentLayout.list")();
    }

    var buildingUpdate = ViewParser.build({
        Url: "pages/building/add.html",
        Var: "buildingUpdate",
        Data: {
            TYPE: "修改",
            Forms: [{
                label: "",
                name: "lfId",
                type: "hidden"
            }, {
                label: "房间编号",
                name: "lfRoomNum",
                type: "text"
            }, {
                label: "所属户型",
                name: "lfHxId",
                type: "mixselect",
                select: DataManager.Observer(function() {
                    var result = [];
                    var apartmentLayout = App.get("Data.apartmentLayout");
                    jQuery.each(apartmentLayout, function(index, apartmentLayoutItem) {
                        result.push({
                            value: apartmentLayoutItem.hxId,
                            key: apartmentLayoutItem.hxName
                        })
                    });
                    return result;
                })
            }, {
                label: "价格",
                name: "lfPrice",
                type: "text"
            }, {
                label: "栋号",
                name: "lfBuildingNum",
                type: "text"
            }, {
                label: "备注",
                name: "lfRemark",
                type: "textArea"
            }],
            Event: {
                submit: function(e, vi) {
                    var dialogContent = jQuery("buildingAddSubmit").html();
                    $.Dialog({
                        overlay: true,
                        shadow: true,
                        draggable: true,
                        icon: '<span class="icon-box-add"></span>',
                        title: '确定修改？',
                        width: 320,
                        padding: 10,
                        content: dialogContent,
                        onShow: function(dialog) {
                            dialog.on("click", ".yes", function() {
                                // Ajax..get("building.add")(buildingUpdate.get("Form"))
                                //     .done(function() {
                                //         dialog.find(".btn-close").click();
                                //     });
                                var form_data = buildingUpdate.get("Forms");
                                var format_data = {};
                                var stopAniamate = false;
                                jQuery.each(form_data, function(index, data) {
                                    format_data[data.name] = data.value;
                                });
                                AjaxData.get("building.update")(format_data, {
                                    success: function(ajaxResult) {
                                        stopAniamate = "success";
                                    },
                                    error: function(ajaxResult) {
                                        stopAniamate = "error"
                                    }
                                });
                                //上传数据的动画
                                var pb = dialog.find("[data-role='progress-bar']").progressbar();
                                var value = 0;
                                var ti = setInterval(function() {
                                    if (stopAniamate = "success") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "修改户型信息成功",
                                            style: {
                                                background: "green",
                                                color: "black"
                                            }
                                        });
                                    } else if (stopAniamate = "error") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "修改户型信息失败",
                                            style: {
                                                background: "darkRed",
                                                color: "white"
                                            }
                                        });
                                    }

                                    try {
                                        value++;
                                        pb.progressbar("value", value % 100);
                                    } catch (e) {
                                        clearInterval(ti);
                                    }
                                }, 20)
                            }).on("click", ".no", function() {
                                dialog.find(".btn-close").click();
                            })
                        }
                    });
                }
            }
        },
    });
    App.set("VIS.building.update", buildingUpdate);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
