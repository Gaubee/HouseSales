ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = [];
    var mutualInit = {}
    AjaxData.get("building.list")();

    var buildingList = ViewParser.build({
        Url: "pages/building/list.html",
        Var: "buildingList",
        Data: [ /*列表数据*/ ],
    });

    App.subset(buildingList, "Data.building");

    App.set("VIS.building.list", buildingList);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
