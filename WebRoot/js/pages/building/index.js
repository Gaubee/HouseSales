ViewParser.ready(function() {

    var buildingIndex = ViewParser.build({
        Url: "pages/building/index.html",
        Var: "buildingIndex",
        Data: {},
    });
    App.set("VIS.building.index", buildingIndex);

});
