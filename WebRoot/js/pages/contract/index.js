ViewParser.ready(function() {

    var contractIndex = ViewParser.build({
        Url: "pages/contract/index.html",
        Var: "contractIndex",
        Data: {},
    });
    App.set("VIS.contract.index", contractIndex);

});
