ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = [];
    var mutualInit = {}
    AjaxData.get("contract.list")();

    var contractList = ViewParser.build({
        Url: "pages/contract/list.html",
        Var: "contractList",
        Data: [ /*列表数据*/ ],
    });

    App.subset(contractList, "Data.contract");

    App.set("VIS.contract.list", contractList);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
