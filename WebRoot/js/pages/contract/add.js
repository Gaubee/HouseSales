ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = ["core", "locale", "calendar", "datepicker" /*,"dialog"*/ , "progressbar", "notify", "input-control"];
    var mutualInit = {
        core: false,
        global: false,
        locale: false,
        calendar: false,
        datepicker: function() {
            console.log("init datepicker");
            $("[data-role='datepicker']").datepicker({
                selected: function(dataStr, dataInstance) {
                    var datepicker = this;
                    var input = datepicker.find("input[type=text]");
                    var bind_key = input.attr("bind-form-key");
                    contractAdd.set(bind_key, dataInstance)
                }
            });
        },
        "input-control": function() {
            jQuery(".input-control").inputControl();
        }
    };

    if (App.get("Data.building").length === 0) {
        AjaxData.get("building.list")();
    }

    if (App.get("Data.customer").length === 0) {
        AjaxData.get("customer.list")();
    }

    if (App.get("Data.salesStaff").length === 0) {
        AjaxData.get("salesStaff.list")();
    }
    var contractAdd = ViewParser.build({
        Url: "pages/contract/add.html",
        Var: "contractAdd",
        Data: {
            TYPE:"添加",
            Forms: [{
                label: "合同编号",
                name: "htId",
                type: "text"
            }, {
                label: "所购买的楼房",
                name: "htLfId",
                type: "mixselect",
                select: DataManager.Observer(function() {
                    var building = App.get("Data.building");
                    var result = [];
                    jQuery.each(building, function(index, buildingItem) {
                        result.push({
                            value: buildingItem.lfId,
                            key: buildingItem.lfRoomNum
                        })
                    });
                    return result;
                })
            }, {
                label: "客户",
                name: "htKhId",
                type: "mixselect",
                select: DataManager.Observer(function() {
                    var customer = App.get("Data.customer");
                    var result = [];
                    jQuery.each(customer, function(index, customerItem) {
                        result.push({
                            value: customerItem.khId,
                            key: customerItem.ubName
                        })
                    });
                    return result;
                })
            }, {
                label: "销售人员",
                name: "htXsId",
                type: "mixselect",
                select: DataManager.Observer(function() {
                    var salesStaff = App.get("Data.salesStaff");
                    var result = [];
                    jQuery.each(salesStaff, function(index, salesStaffItem) {
                        result.push({
                            value: salesStaffItem.xsId,
                            key: salesStaffItem.ubName
                        })
                    });
                    return result;
                })
            }, {
                label: "最终出售价格",
                name: "htSoldPrice",
                type: "text"
            }, {
                label: "付款方式",
                name: "htTermsOfPayment",
                type: "select",
                select: [
                    "一次性付款",
                    "分期付款",
                    "按揭付款",
                    "公积金贷款"
                ]
            }, {
                label: "订购时间",
                name: "htOrderTime",
                type: "date"
            }, {
                label: "购买合约正式签订时间",
                name: "htSigningDate",
                type: "date"
            }, {
                label: "备注",
                name: "htRemark",
                type: "textArea"
            }],
            Event: {
                submit: function(e, vi) {
                    var dialogContent = jQuery("contractAddSubmit").html();
                    $.Dialog({
                        overlay: true,
                        shadow: true,
                        draggable: true,
                        icon: '<span class="icon-box-add"></span>',
                        title: '确定修改？',
                        width: 320,
                        padding: 10,
                        content: dialogContent,
                        onShow: function(dialog) {
                            dialog.on("click", ".yes", function() {
                                // Ajax..get("contract.add")(contractAdd.get("Form"))
                                //     .done(function() {
                                //         dialog.find(".btn-close").click();
                                //     });
                                var form_data = contractAdd.get("Forms");
                                var format_data = {};
                                var stopAniamate = false;
                                jQuery.each(form_data, function(index, data) {
                                    format_data[data.name] = data.value;
                                });
                                AjaxData.get("contract.add")(format_data, {
                                    success: function(ajaxResult) {
                                        stopAniamate = "success";
                                    },
                                    error: function(ajaxResult) {
                                        stopAniamate = "error"
                                    }
                                });
                                //上传数据的动画
                                var pb = dialog.find("[data-role='progress-bar']").progressbar();
                                var value = 0;
                                var ti = setInterval(function() {
                                    if (stopAniamate = "success") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "修改户型信息成功",
                                            style: {
                                                background: "green",
                                                color: "black"
                                            }
                                        });
                                    } else if (stopAniamate = "error") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "修改户型信息失败",
                                            style: {
                                                background: "darkRed",
                                                color: "white"
                                            }
                                        });
                                    }

                                    try {
                                        value++;
                                        pb.progressbar("value", value % 100);
                                    } catch (e) {
                                        clearInterval(ti);
                                    }
                                }, 20)
                            }).on("click", ".no", function() {
                                dialog.find(".btn-close").click();
                            })
                        }
                    });
                }
            }
        },
    });
    App.set("VIS.contract.add", contractAdd);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
