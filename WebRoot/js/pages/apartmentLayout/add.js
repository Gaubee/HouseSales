ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = ["core", "locale", "calendar", "datepicker" /*,"dialog"*/ , "progressbar", "notify", "input-control"];
    var mutualInit = {
        core: false,
        global: false,
        locale: false,
        calendar: false,
        datepicker: function() {
            console.log("init datepicker");
            $("[data-role='datepicker']").datepicker({
                selected: function(dataStr, dataInstance) {
                    var datepicker = this;
                    var input = datepicker.find("input[type=text]");
                    var bind_key = input.attr("bind-form-key");
                    apartmentLayoutAdd.set(bind_key, dataInstance)
                }
            });
        },
        "input-control": function() {
            jQuery(".input-control").inputControl();
        }
    };

    if (App.get("Data.estate").length === 0) {
        AjaxData.get("estate.list")();
    }

    var apartmentLayoutAdd = ViewParser.build({
        Url: "pages/apartmentLayout/add.html",
        Var: "apartmentLayoutAdd",
        Data: {
            TYPE:"添加",
            Forms: [{
                label: "户型名",
                name: "hxName",
                type: "text"
            }, {
                label: "面积",
                name: "hxArea",
                type: "text"
            }, {
                label: "户型类型",
                name: "hxType",
                type: "select",
                select: ["平层户型",
                    "跃层户型",
                    "错层户型",
                    "复式户型"
                ]
            }, {
                label: "所属楼盘",
                name: "hxLpId",
                type: "mixselect",
                select: DataManager.Observer(function() {
                    var result = [];
                    var estate = App.get("Data.estate");
                    jQuery.each(estate, function(index, estateItem) {
                        result.push({
                            value: estateItem.lpId,
                            key: estateItem.lpName
                        })
                    });
                    return result;
                })
            },{
                label: "预览图",
                name: "hxPreview",
                type: "file"
            }, {
                label: "状态",
                name: "hxStatus",
                type: "select",
                select: ["未开售", "销售中", "停售", "售空"]
            }, {
                label: "备注",
                name: "hxRemark",
                type: "textArea"
            }],
            Event: {
                submit: function(e, vi) {
                    var dialogContent = jQuery("apartmentLayoutAddSubmit").html();
                    $.Dialog({
                        overlay: true,
                        shadow: true,
                        draggable: true,
                        icon: '<span class="icon-box-add"></span>',
                        title: '确定添加？',
                        width: 320,
                        padding: 10,
                        content: dialogContent,
                        onShow: function(dialog) {
                            dialog.on("click", ".yes", function() {
                                // Ajax..get("apartmentLayout.add")(apartmentLayoutAdd.get("Form"))
                                //     .done(function() {
                                //         dialog.find(".btn-close").click();
                                //     });
                                var form_data = apartmentLayoutAdd.get("Forms");
                                var format_data = {};
                                var stopAniamate = false;
                                jQuery.each(form_data, function(index, data) {
                                    format_data[data.name] = data.value;
                                });
                                AjaxData.get("apartmentLayout.add")(format_data, {
                                    success: function(ajaxResult) {
                                        stopAniamate = "success";
                                    },
                                    error: function(ajaxResult) {
                                        stopAniamate = "error"
                                    }
                                });
                                //上传数据的动画
                                var pb = dialog.find("[data-role='progress-bar']").progressbar();
                                var value = 0;
                                var ti = setInterval(function() {
                                    if (stopAniamate = "success") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "添加户型信息成功",
                                            style: {
                                                background: "green",
                                                color: "black"
                                            }
                                        });
                                    } else if (stopAniamate = "error") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "添加户型信息失败",
                                            style: {
                                                background: "darkRed",
                                                color: "white"
                                            }
                                        });
                                    }

                                    try {
                                        value++;
                                        pb.progressbar("value", value % 100);
                                    } catch (e) {
                                        clearInterval(ti);
                                    }
                                }, 20)
                            }).on("click", ".no", function() {
                                dialog.find(".btn-close").click();
                            })
                        }
                    });
                }
            }
        },
    });
    App.set("VIS.apartmentLayout.add", apartmentLayoutAdd);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
