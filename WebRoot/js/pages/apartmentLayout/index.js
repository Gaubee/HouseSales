ViewParser.ready(function() {

    var apartmentLayoutIndex = ViewParser.build({
        Url: "pages/apartmentLayout/index.html",
        Var: "apartmentLayoutIndex",
        Data: {},
    });
    App.set("VIS.apartmentLayout.index", apartmentLayoutIndex);

});
