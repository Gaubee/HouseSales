ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = [];
    var mutualInit = {}
    AjaxData.get("apartmentLayout.list")();

    var apartmentLayoutList = ViewParser.build({
        Url: "pages/apartmentLayout/list.html",
        Var: "apartmentLayoutList",
        Data: [ /*列表数据*/ ],
    });

    App.subset(apartmentLayoutList, "Data.apartmentLayout");


    App.set("VIS.apartmentLayout.list", apartmentLayoutList);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
