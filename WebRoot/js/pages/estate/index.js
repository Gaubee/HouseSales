ViewParser.ready(function() {

    var estateIndex = ViewParser.build({
        Url: "pages/estate/index.html",
        Var: "estateIndex",
        Data: {},
    });
    App.set("VIS.estate.index", estateIndex);

});
