ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = [];
    var mutualInit = {}
    AjaxData.get("estate.list")();

    var estateList = ViewParser.build({
        Url: "pages/estate/list.html",
        Var: "estateList",
        Data: [ /*列表数据*/ ],
    });

    App.subset(estateList, "Data.estate");

    App.set("VIS.estate.list", estateList);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
