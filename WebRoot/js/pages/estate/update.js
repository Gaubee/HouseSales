ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = ["core", "locale", "calendar", "datepicker" /*,"dialog"*/ , "progressbar", "notify", "input-control"];
    var mutualInit = {
        core: false,
        global: false,
        locale: false,
        calendar: false,
        datepicker: function() {
            console.log("init datepicker");
            $("[data-role='datepicker']").datepicker({
                selected: function(dataStr, dataInstance) {
                    var datepicker = this;
                    var input = datepicker.find("input[type=text]");
                    var bind_key = input.attr("bind-form-key");
                    estateAdd.set(bind_key, dataInstance)
                }
            });
        },
        "input-control":function(){
            jQuery(".input-control").inputControl();
        }
    };

    var estateAdd = ViewParser.build({
        Url: "pages/estate/add.html",
        Var: "estateAdd",
        Data: {
            TYPE:"修改",
            Forms: [{
                label: "",
                name: "lpId",
                type: "hidden"
            },{
                label: "楼盘名",
                name: "lpName",
                type: "text"
            }, {
                label: "面积",
                name: "lpArea",
                type: "text"
            }, {
                label: "坐标与高度",
                name: "lp3d",
                // type: "text"
                type:"multi-text",
                value:DataManager.Observer(function(key,value){
                    var multi_text = this.get("multi_text");
                    // console.log(this,arguments,multi_text)
                    if (multi_text) {
                        var result = [];
                        jQuery.each(multi_text,function(index,item){
                            // console.log(item)
                            result.push(item.value);
                        })
                    }
                    return result
                }),
                multi_text:[{placeholder:"经"},{placeholder:"纬"},{placeholder:"高"}]
            }, {
                label: "预览图",
                name: "lpPreview",
                type: "file"
            }, {
                label: "楼盘类型",
                name: "lpType",
                type: "select",
                select: ["普通单元式住宅",
                    "公寓式住宅",
                    "复式住宅",
                    "跃层式住宅",
                    "花园洋房式住宅",
                    "小户型住宅"
                ]
            }, {
                label: "备注",
                name: "lpRemark",
                type: "textArea"
            }],
            Event: {
                submit: function(e, vi) {
                    var dialogContent = jQuery("estateAddSubmit").html();
                    $.Dialog({
                        overlay: true,
                        shadow: true,
                        draggable: true,
                        icon: '<span class="icon-box-add"></span>',
                        title: '确定修改？',
                        width: 320,
                        padding: 10,
                        content: dialogContent,
                        onShow: function(dialog) {
                            dialog.on("click", ".yes", function() {
                                // Ajax..get("estate.update")(estateAdd.get("Form"))
                                //     .done(function() {
                                //         dialog.find(".btn-close").click();
                                //     });
                                var form_data = estateAdd.get("Forms");
                                var format_data = {};
                                var stopAniamate = false;
                                jQuery.each(form_data, function(index, data) {
                                    var value = data.value;
                                    if (value instanceof DataManager.Observer) {
                                        value = value.toString();
                                    }
                                    format_data[data.name] = value;
                                });
                                AjaxData.get("estate.update")(format_data, {
                                    success: function(ajaxResult) {
                                        stopAniamate = "success";
                                    },
                                    error: function(ajaxResult) {
                                        stopAniamate = "error"
                                    }
                                });
                                //上传数据的动画
                                var pb = dialog.find("[data-role='progress-bar']").progressbar();
                                var value = 0;
                                var ti = setInterval(function() {
                                    if (stopAniamate = "success") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "修改楼房信息成功",
                                            style: {
                                                background: "green",
                                                color: "black"
                                            }
                                        });
                                    } else if (stopAniamate = "error") {
                                        clearInterval(ti);
                                        dialog.find(".btn-close").click();
                                        $.Notify({
                                            content: "修改楼房信息失败",
                                            style: {
                                                background: "darkRed",
                                                color: "white"
                                            }
                                        });
                                    }

                                    try {
                                        value++;
                                        pb.progressbar("value", value % 100);
                                    } catch (e) {
                                        clearInterval(ti);
                                    }
                                }, 20)
                            }).on("click", ".no", function() {
                                dialog.find(".btn-close").click();
                            })
                        }
                    });
                }
            }
        },
    });
    App.set("VIS.estate.update", estateAdd);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
