ViewParser.ready(function() {

    var customerIndex = ViewParser.build({
        Url: "pages/customer/index.html",
        Var: "customerIndex",
        Data: {},
    });
    App.set("VIS.customer.index", customerIndex);

});
