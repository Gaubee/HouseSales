ViewParser.ready(function() {
    //交互所需的脚本，与初始化程序
    var mutualScripts = [];
    var mutualInit = {}
    AjaxData.get("customer.list")();

    var customerList = ViewParser.build({
        Url: "pages/customer/list.html",
        Var: "customerList",
        Data: [ /*列表数据*/ ],
    });

    App.subset(customerList, "Data.customer");

    App.set("VIS.customer.list", customerList);

    //加载交互所需的脚本并初始化交互
    tool.requireMutual(mutualScripts, mutualInit)
});
