ViewParser.ready(function() {
    function _404() {
        $.Dialog({
            overlay: true,
            shadow: true,
            draggable: true,
            icon: '<span class="icon-rocket"></span>',
            title: 'Router Error',
            width: 500,
            padding: 10,
            content: '错误号404！找不到页面，请检查您访问的页面路径。'
        });
    }

    function _500() {
        $.Dialog({
            overlay: true,
            shadow: true,
            draggable: true,
            icon: '<span class="icon-rocket"></span>',
            title: 'Router Error',
            width: 500,
            padding: 10,
            content: '错误号500！找不到所匹配路由的ViewModel对象！'
        });
    }

    function commentThirdRouter(prefix, type) {
        console.log(type);
        var url = "js/pages/" + prefix + "/" + type + ".js";
        var viKey = ["VIS", prefix, type].join(".");
        var viewInstance = App.get(viKey);

        //路由回调

        function teleporter() {
            console.log("Run teleporter" + type)
            viewInstance || (viewInstance = App.get(viKey));
            if (viewInstance) {
                App.teleporter(viewInstance, "manage");
            } else {
                console.error(viKey + " no ViewModel");
                _500();
            }
        }
        //数据初始化
        console.log(prefix, type)
        if (type === "list") {
            AjaxData.get(prefix + ".list")();
        }
        //页面请求
        if (!viewInstance) {
            console.log(url)
            jQuery.ajax({
                url: url,
                dataType: "text",
                success: function(scriptText) {
                    Function(scriptText)();
                    teleporter();
                },
                error: function() {
                    _404();
                }
            })
        } else {
            teleporter();
        }
    }

    function commentUpdateRouter(prefix, id) {

        console.log(id);
        var url = "js/pages/" + prefix + "/update.js";
        var viKey = ["VIS", prefix, "update"].join(".");
        var viewInstance = App.get(viKey);

        //路由回调
        var findOneData;
        var viewInstance;

        function teleporter() {
            console.log("Run teleporter" + "update")
            if (findOneData) {
                if (viewInstance) {
                    App.teleporter(viewInstance, "manage");
                    var Forms = viewInstance.get("Forms");
                    jQuery.each(Forms, function(index, formsItem) {
                        var value = findOneData[formsItem.name];
                        if (value&&value.time) {
                            //8小时时差
                            value = new Date(value.time + 28800000);
                        }
                        formsItem.value = value;
                    });
                    viewInstance.set("Forms", Forms);
                    viewInstance.dataManager.touchOff("");
                    App.teleporter(viewInstance, "manage");
                } else if (viewInstance === null) {
                    console.error(viKey + " no ViewModel");
                    _500();
                }
            }
        }
        //数据初始化
        console.log(prefix, "update")
        AjaxData.get(prefix + ".findById")({
            id: id
        }, {
            success: function(data) {
                findOneData = data;
                teleporter();
            },
            error: function() {
                $.Dialog({
                    overlay: true,
                    shadow: true,
                    draggable: true,
                    icon: '<span class="icon-rocket"></span>',
                    title: 'Router Error',
                    width: 500,
                    padding: 10,
                    content: '错误号500！找不到所请求的对象！'
                });
                history.back();
            }
        });

        //页面请求
        if (!viewInstance) {
            console.log(url)
            jQuery.ajax({
                url: url,
                dataType: "text",
                success: function(scriptText) {
                    Function(scriptText)();
                    viewInstance = App.get(viKey) || null;
                    teleporter();
                },
                error: function() {
                    _404();
                }
            })
        } else {
            teleporter();
        }
    }

    function commentDeleteRouter(prefix, id) {

        console.log(id);
        var url = "js/pages/" + prefix + "/delete.js";
        var viKey = ["VIS", prefix, "delete"].join(".");
        var viewInstance = App.get(viKey);

        //路由回调

        function teleporter() {
            console.log("Run teleporter" + "delete")
            viewInstance || (viewInstance = App.get(viKey));
            if (viewInstance) {
                App.teleporter(viewInstance, "manage");
            } else {
                console.error(viKey + " no ViewModel");
                _500();
            }
        }
        //数据初始化
        console.log(prefix, "delete")
        AjaxData.get(prefix + ".delete")({
            id: id
        }, {
            success: function() {
                history.back();
                $.Dialog({
                    overlay: true,
                    shadow: true,
                    draggable: true,
                    icon: '<span class="icon-rocket"></span>',
                    title: 'Router Error',
                    width: 500,
                    padding: 10,
                    content: '删除成功'
                });
            },
            error: function() {
                $.Dialog({
                    overlay: true,
                    shadow: true,
                    draggable: true,
                    icon: '<span class="icon-rocket"></span>',
                    title: 'Router Error',
                    width: 500,
                    padding: 10,
                    content: '错误号500！找不到所要删除的对象！'
                });
            }
        });

    }
    routie({
        "!/HouseManage/:nav": function(nav) {
            //房产管理二级导航页
            var prefix = nav.replace("Manage", "");
            prefix = prefix.charAt(0).toLowerCase() + prefix.substr(1);
            commentThirdRouter(prefix,"index");
        },
        "!/HouseManage/:nav/:type": function(nav, type) {
            //房产管理三级页
            var prefix = nav.replace("Manage", "");
            prefix = prefix.charAt(0).toLowerCase() + prefix.substr(1);
            console.log(prefix)
            commentThirdRouter(prefix, type);
        },
        "!/HouseManage/:nav/update/:id": function(nav, id) {
            //房产管理三级页
            var prefix = nav.replace("Manage", "");
            prefix = prefix.charAt(0).toLowerCase() + prefix.substr(1);
            console.log(prefix)
            commentUpdateRouter(prefix, id);
        },
        "!/HouseManage/:nav/delete/:id": function(nav, id) {
            //房产管理三级页
            var prefix = nav.replace("Manage", "");
            prefix = prefix.charAt(0).toLowerCase() + prefix.substr(1);
            console.log(prefix)
            commentDeleteRouter(prefix, id);
        },
        "!/:nav": function(nav) {
            var prefix = nav.replace("Manage", "");
            prefix = prefix.charAt(0).toLowerCase() + prefix.substr(1);
            commentThirdRouter(prefix,"index");
        },
        "!/:nav/:type": function(nav, type) {
            console.log("!/:nav/:type", nav, type)
            var prefix = nav.replace("Manage", "");
            prefix = prefix.charAt(0).toLowerCase() + prefix.substr(1);
            commentThirdRouter(prefix, type);
        },
        "!/:nav/update/:id": function(nav, id) {
            var prefix = nav.replace("Manage", "");
            prefix = prefix.charAt(0).toLowerCase() + prefix.substr(1);
            console.log(prefix)
            commentUpdateRouter(prefix, id);
        },
        "!/:nav/delete/:id": function(nav, id) {
            var prefix = nav.replace("Manage", "");
            prefix = prefix.charAt(0).toLowerCase() + prefix.substr(1);
            console.log(prefix)
            commentDeleteRouter(prefix, id);
        },
        /*
        "!/CustomerManage": function() {
            //客户管理导航页
            function teleporter() {
                console.log(userBaseManageApp)
                App.teleporter(userBaseManageApp, "manage");
            }
            if (typeof userBaseManageApp === "undefined") {
                jQuery.getScript("js/pages/userBase.js", teleporter);
            } else {
                teleporter();
            }
        },
        "!/CustomerManage/:type": function(type) {
            //客户管理二级页
            commentThirdRouter("customer", type)
        },

        "!/ContractManage": function() {
            //合同管理导航页
        },
        "!/ContractManage/:type": function(type) {
            //合同管理二级页
            commentThirdRouter("contract", type)
        },

        "!/SalesStaffManage": function() {
            //销售人员管理导航页
        },
        "!/SalesStaffManage/:type": function(type) {
            //销售人员管理二级页
            commentThirdRouter("salesStaff", type)
        },*/
        "!/*": function(path) {
            _404();
        },
        "*":function(){
            vi = App.get("VIS.index");
            if (!vi) {
                vi = ViewParser.modules["index"]();
                App.set("VIS.index",vi);
            }
            App.teleporter(vi,"manage");
        }
    })
});
