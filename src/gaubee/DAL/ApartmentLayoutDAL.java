package gaubee.DAL;
import gaubee.DAL.BaseDAL;
import java.util.List;

import org.hibernate.*;

import gaubee.model.*;

public class ApartmentLayoutDAL extends BaseDAL{

	public HxApartmentLayout getApartmentLayoutById(String id){
		start();//Start Hibernate
		HxApartmentLayout result = null;
		try{
			Query query = session.createQuery("from HxApartmentLayout where hxId = :id");
			query.setString("id",id);
			List<?> resultList = query.list();
			result = (HxApartmentLayout)resultList.get(0);
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean updateApartmentLayout(HxApartmentLayout apartmentLayout){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.update(apartmentLayout);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean  deleteApartmentLayout(String id){
		start();//Start Hibernate
		boolean result = false;
		try{
			Query query = session.createQuery("delete from HxApartmentLayout where hxId = :id");
			query.setString("id",id);
			query.executeUpdate();
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean addApartmentLayout(HxApartmentLayout apartmentLayout){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.save(apartmentLayout);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public List<HxApartmentLayout> getApartmentLayouts(String condition){
		start();//Start Hibernate
		List<HxApartmentLayout> result = null;
		try{
			Query query = session.createQuery("from HxApartmentLayout "+condition);
			result = query.list();
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public List<HxApartmentLayout> getAllApartmentLayouts(){
		return getApartmentLayouts("");
	}
}
