package gaubee.DAL;
import gaubee.DAL.BaseDAL;
import java.util.List;

import org.hibernate.*;

import gaubee.model.*;

public class SalesStaffDAL extends BaseDAL{

	public XsSalesStaff getSalesStaffById(String id){
		start();//Start Hibernate
		XsSalesStaff result = null;
		try{
			Query query = session.createQuery("from XsSalesStaff where xsId = :id");
			query.setString("id",id);
			List<?> resultList = query.list();
			result = (XsSalesStaff)resultList.get(0);
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean updateSalesStaff(XsSalesStaff salessStaff){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.update(salessStaff);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean destroySalesStaff(String id){
		start();//Start Hibernate
		boolean result = false;
		try{
			XsSalesStaff salessStaff = getSalesStaffById(id);
			
			//删除人物基本信息
			Query query = session.createQuery("delete from UbUserBase where ubId = :id");
			query.setString("id",salessStaff.getXsUbId());
			query.executeUpdate();
			
			//删除与此雇员相关的合同信息（业务记录）
			query = session.createQuery("delete from HtContract where htXsId = :id");
			query.setString("id",salessStaff.getXsId().toString());
			query.executeUpdate();
			
			//删除此雇员
			query = session.createQuery("delete from XsSalesStaff where xsId = :id");
			query.setString("id",id);
			query.executeUpdate();
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	
	public boolean fireSalesStaff(String id){
		start();//Start Hibernate
		boolean result = false;
		try{
			XsSalesStaff salessStaff = getSalesStaffById(id);
			
			//修改此雇员的状态成解雇
			if(!salessStaff.getXsType().contains("|Dismissed|")){
				if(salessStaff.getXsType()==null){
					salessStaff.setXsType("|Dismissed|");
				}else{
					salessStaff.setXsType(salessStaff.getXsType()+"Dismissed|");
				}
				result = updateSalesStaff(salessStaff);
			}
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean addSalesStaff(XsSalesStaff salessStaff){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.save(salessStaff);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public List<XsSalesStaff> getSalesStaffs(String condition){
		start();//Start Hibernate
		List<XsSalesStaff> result = null;
		try{
			Query query = session.createQuery("from XsSalesStaff "+condition);
			result = query.list();
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public List<XsSalesStaff> getAllSalesStaffs(){
		return getSalesStaffs("");
	}
}
