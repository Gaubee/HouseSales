package gaubee.DAL;
import gaubee.DAL.BaseDAL;
import java.util.List;

import org.hibernate.*;

import gaubee.model.*;

public class ContractDAL extends BaseDAL{

	public HtContract getContractById(String id){
		start();//Start Hibernate
		HtContract result = null;
		try{
			Query query = session.createQuery("from HtContract where htId = :id");
			query.setString("id",id);
			List<?> resultList = query.list();
			result = (HtContract)resultList.get(0);
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean updateContract(HtContract contract){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.update(contract);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean  deleteContract(String id){
		start();//Start Hibernate
		boolean result = false;
		try{
			Query query = session.createQuery("delete from HtContract where htId = :id");
			query.setString("id",id);
			query.executeUpdate();
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	
	//预约楼房
	public boolean addContract(HtContract contract){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.save(contract);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}

	public boolean signContract(HtContract contract){
		//修改合同签订的时间戳
		return updateContract(contract);
	}
	public List<HtContract> getContracts(String condition){
		start();//Start Hibernate
		List<HtContract> result = null;
		try{
			Query query = session.createQuery("from HtContract "+condition);
			result = query.list();
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public List<HtContract> getAllContracts(){
		return getContracts("");
	}
}
