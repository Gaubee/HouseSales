package gaubee.DAL;

import gaubee.model.HibernateSessionFactory;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class BaseDAL {

	public Session session ;
	public Transaction ts;
	@SuppressWarnings("unused")
	public void start(){
		session = HibernateSessionFactory.getSession();
		ts = session.beginTransaction();
	}
	@SuppressWarnings("unused")
	public void end(){
		ts.commit();
		HibernateSessionFactory.closeSession();
	}
}
