package gaubee.DAL;
import gaubee.DAL.BaseDAL;
import java.util.List;

import org.hibernate.*;

import gaubee.model.*;

public class UserBaseDAL extends BaseDAL{

	public UbUserBase getUserBaseById(String id){
		start();//Start Hibernate
		UbUserBase result = null;
		try{
			Query query = session.createQuery("from UbUserBase where ubId = :id");
			query.setString("id",id);
			List<?> resultList = query.list();
			result = (UbUserBase)resultList.get(0);
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean updateUserBase(UbUserBase userBase){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.update(userBase);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean  deleteUserBase(String id){
		start();//Start Hibernate
		boolean result = false;
		try{
			Query query = session.createQuery("delete from UbUserBase where ubId = :id");
			query.setString("id",id);
			query.executeUpdate();
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean addUserBase(UbUserBase userBase){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.save(userBase);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public List<UbUserBase> getUserBases(String condition){
		start();//Start Hibernate
		List<UbUserBase> result = null;
		try{
			Query query = session.createQuery("from UbUserBase "+condition);
			result = query.list();
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public List<UbUserBase> getAllUserBases(){
		return getUserBases("");
	}
}
