package gaubee.DAL;
import gaubee.DAL.BaseDAL;
import java.util.List;

import org.hibernate.*;

import gaubee.model.*;

public class CustomerDAL extends BaseDAL{
	public KhCustomer getCustomerById(String id){
		start();//Start Hibernate
		KhCustomer result = null;
		try{
			Query query = session.createQuery("from KhCustomer where khId = :id");
			query.setString("id",id);
			List<?> resultList = query.list();
			result = (KhCustomer)resultList.get(0);
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean updateCustomer(KhCustomer customer){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.update(customer);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean  deleteCustomer(String id){
		start();//Start Hibernate
		boolean result = false;
		try{
			KhCustomer customer = getCustomerById(id);

			Query query = session.createQuery("delete from UbUserBase where ubId = :id");
			query.setString("id",customer.getKhUbId());
			query.executeUpdate();
			
			query = session.createQuery("delete from KhCustomer where khId = :id");
			query.setString("id",id);
			query.executeUpdate();
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean addCustomer(KhCustomer customer){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.save(customer);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public List<KhCustomer> getCustomers(String condition){
		start();//Start Hibernate
		List<KhCustomer> result = null;
		try{
			Query query = session.createQuery("from KhCustomer "+condition);
			result = query.list();
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public List<KhCustomer> getAllCustomers(){
		return getCustomers("");
	}
}
