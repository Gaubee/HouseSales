package gaubee.DAL;
import gaubee.DAL.BaseDAL;
import java.util.List;

import org.hibernate.*;

import gaubee.model.*;

public class BuildingDAL extends BaseDAL{

	public LfBuilding getBuildingById(String id){
		start();//Start Hibernate
		LfBuilding result = null;
		try{
			Query query = session.createQuery("from LfBuilding where lfId = :id");
			query.setString("id",id);
			List<?> resultList = query.list();
			result = (LfBuilding)resultList.get(0);
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean updateBuilding(LfBuilding building){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.update(building);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean  deleteBuilding(String id){
		start();//Start Hibernate
		boolean result = false;
		try{
			Query query = session.createQuery("delete from LfBuilding where lfId = :id");
			query.setString("id",id);
			query.executeUpdate();
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean addBuilding(LfBuilding building){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.save(building);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public List<LfBuilding> getBuildings(String condition){
		start();//Start Hibernate
		List<LfBuilding> result = null;
		try{
			Query query = session.createQuery("from LfBuilding "+condition);
			result = query.list();
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public List<LfBuilding> getAllBuildings(){
		return getBuildings("");
	}
}
