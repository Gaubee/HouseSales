package gaubee.DAL;
import gaubee.DAL.BaseDAL;
import java.util.List;

import org.hibernate.*;

import gaubee.model.*;

public class EstateDAL extends BaseDAL{

	public LpEstate getEstateById(String id){
		start();//Start Hibernate
		LpEstate result = null;
		try{
			Query query = session.createQuery("from LpEstate where lpId = :id");
			query.setString("id",id);
			List<?> resultList = query.list();
			result = (LpEstate)resultList.get(0);
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean updateEstate(LpEstate building){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.update(building);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean  deleteEstate(String id){
		start();//Start Hibernate
		boolean result = false;
		try{
			Query query = session.createQuery("delete from LpEstate where lpId = :id");
			query.setString("id",id);
			query.executeUpdate();
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public boolean addEstate(LpEstate building){
		start();//Start Hibernate
		boolean result = false;
		try{
			session.save(building);
			result = true;
		}catch (Exception e){e.printStackTrace();
			result = false;
		}
		end();//Close Hibernate
		return result;
	}
	public List<LpEstate> getEstates(String condition){
		start();//Start Hibernate
		List<LpEstate> result = null;
		try{
			Query query = session.createQuery("from LpEstate "+condition);
			result = query.list();
		}catch (Exception e){e.printStackTrace();
			result = null;
		}
		end();//Close Hibernate
		return result;
	}
	public List<LpEstate> getAllEstates(){
		return getEstates("");
	}
}
