package gaubee.BLL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import gaubee.model.*;
import gaubee.DAL.*;
import gaubee.BLL.*;
import javax.servlet.*;

import net.sf.json.*;

public class CustomerBLL extends BaseBLL{
	private KhCustomerDAO customerDAO = new KhCustomerDAO();
	private UbUserBaseDAO userbaseDAO = new UbUserBaseDAO();
	static public KhCustomer parameterToCustomer(ServletRequest req){
		KhCustomer customer = new KhCustomer();
		if(req.getParameter("khId")!=null){
			customer.setKhId(Integer.parseInt(req.getParameter("khId")));
		}
		customer.setKhRemark(req.getParameter("khRemark"));
		customer.setKhType(req.getParameter("khType"));
		customer.setKhUbId(req.getParameter("KhUbId"));//KhUbId
		if(customer.getKhUbId() == null){
			customer.setKhUbId(req.getParameter("ubId"));
		}
		return customer;
	}
	public void run(ServletRequest req, ServletResponse res) throws IOException{
		PrintWriter pw = res.getWriter();
		String router = req.getParameter("method");

		if (router.equals("findById")){
			System.out.println("findById");
			findById(req,pw);
		}else if (router.equals("findAll")){
			System.out.println("findAll");
			findAll(req,pw);
		}else if (router.equals("findByProperty")){
			System.out.println("findByProperty");
			findByProperty(req,pw);
		}else if (router.equals("insert")){
			System.out.println("insert");
			insert(req,pw);
		}else if (router.equals("update")){
			System.out.println("update");
			update(req,pw);
		}else if (router.equals("delete")){
			System.out.println("delete");
			delete(req,pw);
		}
		System.out.println("End UserBase BLL!!");
	} 

	public HashMap mergeCustomerAndUserBase(KhCustomer customer){
		HashMap result = new HashMap();
		UbUserBase userbase = userbaseDAO.findById(customer.getKhUbId());

		result.put("khId",customer.getKhId());
		result.put("khUbId",customer.getKhUbId());
		result.put("khType",customer.getKhType());
		result.put("khRemark",customer.getKhRemark());

		result.put("ubName",userbase.getUbName());
		result.put("ubSex",userbase.getUbSex());
		result.put("ubTel",userbase.getUbTel());
		result.put("ubBirthDay",userbase.getUbBirthDay());
		result.put("ubAddress",userbase.getUbAddress());
		result.put("ubEmail",userbase.getUbEmail());
		result.put("ubSns",userbase.getUbSns());
		result.put("ubRemark",userbase.getUbRemark());

		return result;
	}

	private HashMap findById(ServletRequest req,PrintWriter pw){
		KhCustomer customer = customerDAO.findById(Integer.parseInt(req.getParameter("id")));
		HashMap result = mergeCustomerAndUserBase(customer);
		writeJSONstringify(result,pw);
		return result;
	}
	private List<HashMap> findAll(ServletRequest req,PrintWriter pw){
		List<KhCustomer> customerList = customerDAO.findAll();
		List<HashMap> result = new ArrayList<HashMap>();
		for(KhCustomer customerItem:customerList){
			result.add(mergeCustomerAndUserBase(customerItem));
		}
		writeJSONstringify(result,pw);
		return result;
	}
	private List<KhCustomer> findByProperty(ServletRequest req,PrintWriter pw){
		List<KhCustomer> result = customerDAO.findByProperty(req.getParameter("key"),req.getParameter("value"));
		writeJSONstringify(result,pw);
		return result;
	}
	private boolean insert(ServletRequest req,PrintWriter pw){
		KhCustomer newCustomer = parameterToCustomer(req);
		UbUserBase oldUserbase = userbaseDAO.findById(newCustomer.getKhUbId());
		//身份证号码已经存在，转为修改
		if(oldUserbase!=null){
			//或许可能已经存在的客户
			KhCustomer oldCustomer = (KhCustomer) customerDAO.findByProperty("khUbId",newCustomer.getKhUbId()).get(0);
			if(oldCustomer!=null){
				//存在的话就定义主键，转为修改
				newCustomer.setKhId(oldCustomer.getKhId());
			}
			customerDAO.merge(newCustomer);
			userbaseDAO.merge(UserBaseBLL.parameterToUserBase(req));
		}else{
			customerDAO.save(parameterToCustomer(req));
			userbaseDAO.save(UserBaseBLL.parameterToUserBase(req));
		}
		boolean result = true;
		writeJSONstringify(simpleResult(true,"insert customer","success"),pw);
		return result;
	}
	private boolean update(ServletRequest req,PrintWriter pw) {
		customerDAO.merge(parameterToCustomer(req));
		userbaseDAO.merge(UserBaseBLL.parameterToUserBase(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"update customer","success"),pw);
		return result;
	}
	private boolean delete(ServletRequest req,PrintWriter pw){
		customerDAO.delete(customerDAO.findById(Integer.parseInt(req.getParameter("id"))));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"delete customer","success"),pw);
		return result;
	}
}
