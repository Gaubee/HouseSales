package gaubee.BLL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import gaubee.model.*;
import gaubee.DAL.*;
import gaubee.BLL.*;
import javax.servlet.*;

import net.sf.json.*;

public class UserBaseBLL extends BaseBLL{
	private UserBaseDAL userBaseDAL = new UserBaseDAL();
	static public UbUserBase parameterToUserBase(ServletRequest req){
		UbUserBase userBase = new UbUserBase();

		if (req.getParameter("khUbId")!=null) {
			userBase.setUbId(req.getParameter("khUbId"));
		}else if (req.getParameter("xsUbId")!=null) {
			userBase.setUbId(req.getParameter("xsUbId"));
		}
		if(userBase.getUbId()==null){
			userBase.setUbId(req.getParameter("ubId"));
		}

		
		userBase.setUbName(req.getParameter("ubName"));
		userBase.setUbSex(req.getParameter("ubSex"));
		userBase.setUbTel(req.getParameter("ubTel"));
		if(req.getParameter("ubBirthDay")!=null){
			userBase.setUbBirthDay(StringToTimestamp(req.getParameter("ubBirthDay")));
		}
		userBase.setUbAddress(req.getParameter("ubAddress"));
		userBase.setUbEmail(req.getParameter("ubEmail"));
		userBase.setUbSns(req.getParameter("ubSns"));
		userBase.setUbRemark(req.getParameter("ubRemark"));
		return userBase;
	}
	public void run(ServletRequest req, ServletResponse res) throws IOException{
		PrintWriter pw = res.getWriter();
		String router = req.getParameter("method");
		

		//自定义路由
		if (router.equals("getById")){
			System.out.println("getById");
			getById(req,pw);
		}else if (router.equals("getAll")){
			System.out.println("getAll");
			getAll(req,pw);
		}else if (router.equals("getByCon")){
			System.out.println("getByCon");
			getByCondition(req,pw);
		}else if (router.equals("add")){
			System.out.println("add");
			add(req,pw);
		}else if (router.equals("update")){
			System.out.println("update");
			update(req,pw);
		}else if (router.equals("delete")){
			System.out.println("delete");
			delete(req,pw);
		}
		System.out.println("End UserBase BLL!!");
	} 
	private UbUserBase getById(ServletRequest req,PrintWriter pw){
		UbUserBase result = userBaseDAL.getUserBaseById(req.getParameter("id"));
		JSONObject jsonResult = JSONObject.fromObject(result);
		pw.println(jsonResult);
		return result;
	}
	private List<UbUserBase> getAll(ServletRequest req,PrintWriter pw){
		List<UbUserBase> result = userBaseDAL.getAllUserBases();
		JSONArray jsonResult = JSONArray.fromObject(result);
		pw.println(jsonResult);
		return result;
	}
	private List<UbUserBase> getByCondition(ServletRequest req,PrintWriter pw){
		List<UbUserBase> result = userBaseDAL.getUserBases(req.getParameter("con"));
		JSONArray jsonResult = JSONArray.fromObject(result);
		pw.println(jsonResult);
		return result;
	}
	private boolean add(ServletRequest req,PrintWriter pw){
		boolean result = userBaseDAL.addUserBase(parameterToUserBase(req));
		Map mapResult = new HashMap();
		mapResult.put("result",true);
		mapResult.put("method","addUserBase");
		mapResult.put("message","success");
		JSONObject jsonResult = JSONObject.fromObject(mapResult);
		pw.println(jsonResult);
		return result;
	}
	private boolean update(ServletRequest req,PrintWriter pw) {
		boolean result = userBaseDAL.updateUserBase(parameterToUserBase(req));
		Map mapResult = new HashMap();
		mapResult.put("result",true);
		mapResult.put("method","addUserBase");
		mapResult.put("message","success");
		JSONObject jsonResult = JSONObject.fromObject(mapResult);
		pw.println(jsonResult);
		return result;
	}
	private boolean delete(ServletRequest req,PrintWriter pw){
		boolean result = userBaseDAL.deleteUserBase(req.getParameter("id"));
		Map mapResult = new HashMap();
		mapResult.put("result",true);
		mapResult.put("method","addUserBase");
		mapResult.put("message","success");
		JSONObject jsonResult = JSONObject.fromObject(mapResult);
		pw.println(jsonResult);
		return result;
	}
}
