package gaubee.BLL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import gaubee.model.*;
import gaubee.DAL.*;
import gaubee.BLL.*;
import javax.servlet.*;

import net.sf.json.*;

public class ContractBLL extends BaseBLL{
	private HtContractDAO contractDAO = new HtContractDAO();
	private UbUserBaseDAO userbaseDAO = new UbUserBaseDAO();
	static public HtContract parameterToCustomer(ServletRequest req){
		HtContract contract = new HtContract();

		contract.setHtId(req.getParameter("htId"));

		if(req.getParameter("htLfId")!=null){
			contract.setHtLfId(Integer.parseInt(req.getParameter("htLfId")));
		}
		if(req.getParameter("htKhId")!=null){
			contract.setHtKhId(Integer.parseInt(req.getParameter("htKhId")));
		}
		if(req.getParameter("htXsId")!=null){
			contract.setHtXsId(Integer.parseInt(req.getParameter("htXsId")));
		}

		if(req.getParameter("htSoldPrice")!=null){
			contract.setHtSoldPrice(Double.parseDouble(req.getParameter("htSoldPrice")));
		}

		contract.setHtTermsOfPayment(req.getParameter("htTermsOfPayment"));

		if(req.getParameter("htOrderTime")!=null){
			contract.setHtOrderTime(StringToTimestamp(req.getParameter("htOrderTime")));
		}
		if(req.getParameter("htSigningDate")!=null){
			contract.setHtSigningDate(StringToTimestamp(req.getParameter("htSigningDate")));
		}

		contract.setHtRemark(req.getParameter("htRemark"));

		return contract;
	}
	public void run(ServletRequest req, ServletResponse res) throws IOException{
		PrintWriter pw = res.getWriter();
		String router = req.getParameter("method");

		if (router.equals("findById")){
			System.out.println("findById");
			findById(req,pw);
		}else if (router.equals("findAll")){
			System.out.println("findAll");
			findAll(req,pw);
		}else if (router.equals("findByProperty")){
			System.out.println("findByProperty");
			findByProperty(req,pw);
		}else if (router.equals("insert")){
			System.out.println("insert");
			insert(req,pw);
		}else if (router.equals("update")){
			System.out.println("update");
			update(req,pw);
		}else if (router.equals("delete")){
			System.out.println("delete");
			delete(req,pw);
		}
		System.out.println("End UserBase BLL!!");
	} 

	private HtContract findById(ServletRequest req,PrintWriter pw){
		HtContract result = contractDAO.findById(req.getParameter("id"));
		writeJSONstringify(result,pw);
		return result;
	}
	private List<HtContract> findAll(ServletRequest req,PrintWriter pw){
		List<HtContract> result = contractDAO.findAll();
		writeJSONstringify(result,pw);
		return result;
	}
	private List<HtContract> findByProperty(ServletRequest req,PrintWriter pw){
		List<HtContract> result = contractDAO.findByProperty(req.getParameter("key"),req.getParameter("value"));
		writeJSONstringify(result,pw);
		return result;
	}
	private boolean insert(ServletRequest req,PrintWriter pw){
		contractDAO.save(parameterToCustomer(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"insert contract","success"),pw);
		return result;
	}
	private boolean update(ServletRequest req,PrintWriter pw) {
		contractDAO.merge(parameterToCustomer(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"update contract","success"),pw);
		return result;
	}
	private boolean delete(ServletRequest req,PrintWriter pw){
		contractDAO.delete(contractDAO.findById(req.getParameter("id")));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"delete contract","success"),pw);
		return result;
	}
}
