package gaubee.BLL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import gaubee.model.*;
import gaubee.DAL.*;
import gaubee.BLL.*;
import javax.servlet.*;

import net.sf.json.*;

public class ApartmentLayoutBLL extends BaseBLL{
	private static HxApartmentLayoutDAO apartmentLayoutDAO = new HxApartmentLayoutDAO();
	private static LfBuildingDAO buildingDAO = new LfBuildingDAO();
	private static UbUserBaseDAO userbaseDAO = new UbUserBaseDAO();
	
	static public void deleteWithChilds(int apartmentLayoutId){
		HxApartmentLayout currentApartmentLayout = apartmentLayoutDAO.findById(apartmentLayoutId);

		List<LfBuilding> buildingList = buildingDAO.findByProperty("lfHxId",apartmentLayoutId);
		for(LfBuilding buildingListItem:buildingList){
			buildingDAO.delete(buildingListItem);
		}
		//deleteWithChilds(currentApartmentLayout.getHxId());
		apartmentLayoutDAO.delete(currentApartmentLayout);

	}
	
	static public HxApartmentLayout parameterToCustomer(ServletRequest req){
		HxApartmentLayout apartmentLayout = new HxApartmentLayout();
		if(req.getParameter("hxId")!=null){
			apartmentLayout.setHxId(Integer.parseInt(req.getParameter("hxId")));
		}
		if(req.getParameter("hxLpId")!=null){
			apartmentLayout.setHxLpId(Integer.parseInt(req.getParameter("hxLpId")));
		}
		apartmentLayout.setHxName(req.getParameter("hxName"));
		if(req.getParameter("hxArea")!=null){
			apartmentLayout.setHxArea(Double.parseDouble(req.getParameter("hxArea")));
		}
		apartmentLayout.setHxType(req.getParameter("hxType"));
		apartmentLayout.setHxPreview(req.getParameter("hxPreview"));
		apartmentLayout.setHxStatus(req.getParameter("hxStatus"));
		apartmentLayout.setHxRemark(req.getParameter("hxRemark"));

		return apartmentLayout;
	}
	public void run(ServletRequest req, ServletResponse res) throws IOException{
		PrintWriter pw = res.getWriter();
		String router = req.getParameter("method");

		if (router.equals("findById")){
			System.out.println("findById");
			findById(req,pw);
		}else if (router.equals("findAll")){
			System.out.println("findAll");
			findAll(req,pw);
		}else if (router.equals("findByProperty")){
			System.out.println("findByProperty");
			findByProperty(req,pw);
		}else if (router.equals("insert")){
			System.out.println("insert");
			insert(req,pw);
		}else if (router.equals("update")){
			System.out.println("update");
			update(req,pw);
		}else if (router.equals("delete")){
			System.out.println("delete");
			delete(req,pw);
		}
		System.out.println("End UserBase BLL!!");
	} 

	private HxApartmentLayout findById(ServletRequest req,PrintWriter pw){
		HxApartmentLayout result = apartmentLayoutDAO.findById(Integer.parseInt(req.getParameter("id")));
		writeJSONstringify(result,pw);
		return result;
	}
	private List<HxApartmentLayout> findAll(ServletRequest req,PrintWriter pw){
		List<HxApartmentLayout> result = apartmentLayoutDAO.findAll();
		writeJSONstringify(result,pw);
		return result;
	}
	private List<HxApartmentLayout> findByProperty(ServletRequest req,PrintWriter pw){
		List<HxApartmentLayout> result = apartmentLayoutDAO.findByProperty(req.getParameter("key"),req.getParameter("value"));
		writeJSONstringify(result,pw);
		return result;
	}
	private boolean insert(ServletRequest req,PrintWriter pw){
		apartmentLayoutDAO.save(parameterToCustomer(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"insert apartmentLayout","success"),pw);
		return result;
	}
	private boolean update(ServletRequest req,PrintWriter pw) {
		apartmentLayoutDAO.merge(parameterToCustomer(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"update apartmentLayout","success"),pw);
		return result;
	}
	private boolean delete(ServletRequest req,PrintWriter pw){

		deleteWithChilds(Integer.parseInt(req.getParameter("id")));

		boolean result = true;
		writeJSONstringify(simpleResult(true,"delete apartmentLayout","success"),pw);
		return result;
	}
}
