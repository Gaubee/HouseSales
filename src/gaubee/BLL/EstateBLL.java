package gaubee.BLL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import gaubee.model.*;
import gaubee.DAL.*;
import gaubee.BLL.*;
import javax.servlet.*;

import net.sf.json.*;

public class EstateBLL extends BaseBLL{
	private static LpEstateDAO estateDAO = new LpEstateDAO();
	private static UbUserBaseDAO userbaseDAO = new UbUserBaseDAO();
	private static HxApartmentLayoutDAO apartmentLayoutDAO = new HxApartmentLayoutDAO();
	
	static public void deleteWithChilds(int estateId){
		LpEstate currentEstate = estateDAO.findById(estateId);


		List<HxApartmentLayout> apartmentLayoutList = apartmentLayoutDAO.findByProperty("hxLpId",estateId);
		for(HxApartmentLayout apartmentLayoutListItem:apartmentLayoutList){
			//apartmentLayoutDAO.delete(apartmentLayoutListItem);
			ApartmentLayoutBLL.deleteWithChilds(apartmentLayoutListItem.getHxId());
		}
		//deleteWithChilds(currentEstate.getLpId());
		estateDAO.delete(currentEstate);
	}
	
	static public LpEstate parameterToCustomer(ServletRequest req){
		LpEstate estate = new LpEstate();
		if(req.getParameter("lpId")!=null){
			estate.setLpId(Integer.parseInt(req.getParameter("lpId")));
		}
		estate.setLpName(req.getParameter("lpName"));
		if(req.getParameter("lpArea")!=null){
			estate.setLpArea(Double.parseDouble(req.getParameter("lpArea")));
		}
		estate.setLp3d(req.getParameter("lp3d"));
		estate.setLpPreview(req.getParameter("lpPreview"));
		estate.setLpType(req.getParameter("lpType"));
		estate.setLpRemark(req.getParameter("lpRemark"));
		return estate;
	}
	public void run(ServletRequest req, ServletResponse res) throws IOException{
		PrintWriter pw = res.getWriter();
		String router = req.getParameter("method");

		if (router.equals("findById")){
			System.out.println("findById");
			findById(req,pw);
		}else if (router.equals("findAll")){
			System.out.println("findAll");
			findAll(req,pw);
		}else if (router.equals("findByProperty")){
			System.out.println("findByProperty");
			findByProperty(req,pw);
		}else if (router.equals("insert")){
			System.out.println("insert");
			insert(req,pw);
		}else if (router.equals("update")){
			System.out.println("update");
			update(req,pw);
		}else if (router.equals("delete")){
			System.out.println("delete");
			delete(req,pw);
		}
		System.out.println("End UserBase BLL!!");
	} 

	private LpEstate findById(ServletRequest req,PrintWriter pw){
		LpEstate result = estateDAO.findById(Integer.parseInt(req.getParameter("id")));
		writeJSONstringify(result,pw);
		return result;
	}
	private List<LpEstate> findAll(ServletRequest req,PrintWriter pw){
		List<LpEstate> result = estateDAO.findAll();
		writeJSONstringify(result,pw);
		return result;
	}
	private List<LpEstate> findByProperty(ServletRequest req,PrintWriter pw){
		List<LpEstate> result = estateDAO.findByProperty(req.getParameter("key"),req.getParameter("value"));
		writeJSONstringify(result,pw);
		return result;
	}
	private boolean insert(ServletRequest req,PrintWriter pw){
		estateDAO.save(parameterToCustomer(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"insert estate","success"),pw);
		return result;
	}
	private boolean update(ServletRequest req,PrintWriter pw) {
		estateDAO.merge(parameterToCustomer(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"update estate","success"),pw);
		return result;
	}
	private boolean delete(ServletRequest req,PrintWriter pw){
		//estateDAO.delete(estateDAO.findById(Integer.parseInt(req.getParameter("id"))));
		
		deleteWithChilds(Integer.parseInt(req.getParameter("id")));
		
		boolean result = true;
		writeJSONstringify(simpleResult(true,"delete estate","success"),pw);
		return result;
	}
}
