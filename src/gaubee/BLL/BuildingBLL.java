package gaubee.BLL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import gaubee.model.*;
import gaubee.DAL.*;
import gaubee.BLL.*;
import javax.servlet.*;

import net.sf.json.*;

public class BuildingBLL extends BaseBLL{
	private LfBuildingDAO buildingDAO = new LfBuildingDAO();
	private UbUserBaseDAO userbaseDAO = new UbUserBaseDAO();
	static public LfBuilding parameterToCustomer(ServletRequest req){
		LfBuilding building = new LfBuilding();
		if(req.getParameter("lfId")!=null){
			building.setLfId(Integer.parseInt(req.getParameter("lfId")));
		}
		if(req.getParameter("lfHxId")!=null){
			building.setLfHxId(Integer.parseInt(req.getParameter("lfHxId")));
		}
		if(req.getParameter("lfPrice")!=null){
			building.setLfPrice(Double.parseDouble(req.getParameter("lfPrice")));
		}
		building.setLfRoomNum(req.getParameter("lfRoomNum"));
		building.setLfBuildingNum(req.getParameter("lfBuildingNum"));
		building.setLfRemark(req.getParameter("lfRemark"));

		return building;
	}
	public void run(ServletRequest req, ServletResponse res) throws IOException{
		PrintWriter pw = res.getWriter();
		String router = req.getParameter("method");

		if (router.equals("findById")){
			System.out.println("findById");
			findById(req,pw);
		}else if (router.equals("findAll")){
			System.out.println("findAll");
			findAll(req,pw);
		}else if (router.equals("findByProperty")){
			System.out.println("findByProperty");
			findByProperty(req,pw);
		}else if (router.equals("insert")){
			System.out.println("insert");
			insert(req,pw);
		}else if (router.equals("update")){
			System.out.println("update");
			update(req,pw);
		}else if (router.equals("delete")){
			System.out.println("delete");
			delete(req,pw);
		}
		System.out.println("End UserBase BLL!!");
	} 

	private LfBuilding findById(ServletRequest req,PrintWriter pw){
		LfBuilding result = buildingDAO.findById(Integer.parseInt(req.getParameter("id")));
		writeJSONstringify(result,pw);
		return result;
	}
	private List<LfBuilding> findAll(ServletRequest req,PrintWriter pw){
		List<LfBuilding> result = buildingDAO.findAll();
		writeJSONstringify(result,pw);
		return result;
	}
	private List<LfBuilding> findByProperty(ServletRequest req,PrintWriter pw){
		List<LfBuilding> result = buildingDAO.findByProperty(req.getParameter("key"),req.getParameter("value"));
		writeJSONstringify(result,pw);
		return result;
	}
	private boolean insert(ServletRequest req,PrintWriter pw){
		buildingDAO.save(parameterToCustomer(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"insert building","success"),pw);
		return result;
	}
	private boolean update(ServletRequest req,PrintWriter pw) {
		buildingDAO.merge(parameterToCustomer(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"update building","success"),pw);
		return result;
	}
	private boolean delete(ServletRequest req,PrintWriter pw){
		buildingDAO.delete(buildingDAO.findById(Integer.parseInt(req.getParameter("id"))));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"delete building","success"),pw);
		return result;
	}
}
