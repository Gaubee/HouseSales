package gaubee.BLL;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import gaubee.model.*;
import gaubee.DAL.*;
import gaubee.BLL.*;
import javax.servlet.*;

import net.sf.json.*;

public class SalesStaffBLL extends BaseBLL{
	private XsSalesStaffDAO salesStaffDAO = new XsSalesStaffDAO();
	private UbUserBaseDAO userbaseDAO = new UbUserBaseDAO();
	static public XsSalesStaff parameterToCustomer(ServletRequest req){
		XsSalesStaff salesStaff = new XsSalesStaff();
		if(req.getParameter("xsId")!=null){
			salesStaff.setXsId(Integer.parseInt(req.getParameter("xsId")));
		}
		salesStaff.setXsRemark(req.getParameter("xsRemark"));
		salesStaff.setXsType(req.getParameter("xsType"));
		salesStaff.setXsUbId(req.getParameter("xsUbId"));//XsUbId
		if(salesStaff.getXsUbId()==null){
			salesStaff.setXsUbId(req.getParameter("ubId"));
		}
		return salesStaff;
	}
	public void run(ServletRequest req, ServletResponse res) throws IOException{
		PrintWriter pw = res.getWriter();
		String router = req.getParameter("method");

		if (router.equals("findById")){
			System.out.println("findById");
			findById(req,pw);
		}else if (router.equals("findAll")){
			System.out.println("findAll");
			findAll(req,pw);
		}else if (router.equals("findByProperty")){
			System.out.println("findByProperty");
			findByProperty(req,pw);
		}else if (router.equals("insert")){
			System.out.println("insert");
			insert(req,pw);
		}else if (router.equals("update")){
			System.out.println("update");
			update(req,pw);
		}else if (router.equals("delete")){
			System.out.println("delete");
			delete(req,pw);
		}
		System.out.println("End UserBase BLL!!");
	} 

	public HashMap mergeCustomerAndUserBase(XsSalesStaff salesStaff){
		HashMap result = new HashMap();
		UbUserBase userbase = userbaseDAO.findById(salesStaff.getXsUbId());

		result.put("xsId",salesStaff.getXsId());
		result.put("xsUbId",salesStaff.getXsUbId());
		result.put("xsType",salesStaff.getXsType());
		result.put("xsRemark",salesStaff.getXsRemark());

		result.put("ubName",userbase.getUbName());
		result.put("ubSex",userbase.getUbSex());
		result.put("ubTel",userbase.getUbTel());
		result.put("ubBirthDay",userbase.getUbBirthDay());
		result.put("ubAddress",userbase.getUbAddress());
		result.put("ubEmail",userbase.getUbEmail());
		result.put("ubSns",userbase.getUbSns());
		result.put("ubRemark",userbase.getUbRemark());

		return result;
	}

	private HashMap findById(ServletRequest req,PrintWriter pw){
		XsSalesStaff salesStaff = salesStaffDAO.findById(Integer.parseInt(req.getParameter("id")));
		HashMap result = mergeCustomerAndUserBase(salesStaff);
		writeJSONstringify(result,pw);
		return result;
	}
	private List<HashMap> findAll(ServletRequest req,PrintWriter pw){
		List<XsSalesStaff> salesStaffList = salesStaffDAO.findAll();
		List<HashMap> result = new ArrayList<HashMap>();
		for(XsSalesStaff salesStaffItem:salesStaffList){
			result.add(mergeCustomerAndUserBase(salesStaffItem));
		}
		writeJSONstringify(result,pw);
		return result;
	}
	private List<XsSalesStaff> findByProperty(ServletRequest req,PrintWriter pw){
		List<XsSalesStaff> result = salesStaffDAO.findByProperty(req.getParameter("key"),req.getParameter("value"));
		writeJSONstringify(result,pw);
		return result;
	}
	private boolean insert(ServletRequest req,PrintWriter pw){
		XsSalesStaff newSalesStaff = parameterToCustomer(req);
		UbUserBase oldUserbase = userbaseDAO.findById(newSalesStaff.getXsUbId());
		//身份证号码已经存在，转为修改
		if(oldUserbase!=null){
			//或许可能已经存在的销售人员
			XsSalesStaff oldSalesStaff = (XsSalesStaff) salesStaffDAO.findByProperty("xsUbId",newSalesStaff.getXsUbId()).get(0);
			if(oldSalesStaff!=null){
				//存在的话就定义主键，转为修改
				newSalesStaff.setXsId(oldSalesStaff.getXsId());
			}
			salesStaffDAO.merge(newSalesStaff);
			userbaseDAO.merge(UserBaseBLL.parameterToUserBase(req));
		}else{
			salesStaffDAO.save(parameterToCustomer(req));
			userbaseDAO.save(UserBaseBLL.parameterToUserBase(req));
		}
		boolean result = true;
		writeJSONstringify(simpleResult(true,"insert salesStaff","success"),pw);
		return result;
	}
	private boolean update(ServletRequest req,PrintWriter pw) {
		salesStaffDAO.merge(parameterToCustomer(req));
		userbaseDAO.merge(UserBaseBLL.parameterToUserBase(req));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"update salesStaff","success"),pw);
		return result;
	}
	private boolean delete(ServletRequest req,PrintWriter pw){
		salesStaffDAO.delete(salesStaffDAO.findById(Integer.parseInt(req.getParameter("id"))));
		boolean result = true;
		writeJSONstringify(simpleResult(true,"delete salesStaff","success"),pw);
		return result;
	}
}
