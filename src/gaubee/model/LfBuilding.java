package gaubee.model;

/**
 * LfBuilding entity. @author MyEclipse Persistence Tools
 */
public class LfBuilding extends AbstractLfBuilding implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public LfBuilding() {
	}

	/** full constructor */
	public LfBuilding(Integer lfHxId, String lfRoomNum, String lfBuildingNum,
			Double lfPrice, String lfRemark) {
		super(lfHxId, lfRoomNum, lfBuildingNum, lfPrice, lfRemark);
	}

}
