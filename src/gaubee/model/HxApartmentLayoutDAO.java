package gaubee.model;

import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * HxApartmentLayout entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see gaubee.model.HxApartmentLayout
 * @author MyEclipse Persistence Tools
 */

public class HxApartmentLayoutDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(HxApartmentLayoutDAO.class);
	// property constants
	public static final String HX_LP_ID = "hxLpId";
	public static final String HX_NAME = "hxName";
	public static final String HX_AREA = "hxArea";
	public static final String HX_TYPE = "hxType";
	public static final String HX_PREVIEW = "hxPreview";
	public static final String HX_STATUS = "hxStatus";
	public static final String HX_REMARK = "hxRemark";

	public void save(HxApartmentLayout transientInstance) {
		log.debug("saving HxApartmentLayout instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(HxApartmentLayout persistentInstance) {
		log.debug("deleting HxApartmentLayout instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public HxApartmentLayout findById(java.lang.Integer id) {
		log.debug("getting HxApartmentLayout instance with id: " + id);
		try {
			HxApartmentLayout instance = (HxApartmentLayout) getSession().get(
					"gaubee.model.HxApartmentLayout", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(HxApartmentLayout instance) {
		log.debug("finding HxApartmentLayout instance by example");
		try {
			List results = getSession().createCriteria(
					"gaubee.model.HxApartmentLayout").add(
					Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding HxApartmentLayout instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from HxApartmentLayout as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByHxLpId(Object hxLpId) {
		return findByProperty(HX_LP_ID, hxLpId);
	}

	public List findByHxName(Object hxName) {
		return findByProperty(HX_NAME, hxName);
	}

	public List findByHxArea(Object hxArea) {
		return findByProperty(HX_AREA, hxArea);
	}

	public List findByHxType(Object hxType) {
		return findByProperty(HX_TYPE, hxType);
	}

	public List findByHxPreview(Object hxPreview) {
		return findByProperty(HX_PREVIEW, hxPreview);
	}

	public List findByHxStatus(Object hxStatus) {
		return findByProperty(HX_STATUS, hxStatus);
	}

	public List findByHxRemark(Object hxRemark) {
		return findByProperty(HX_REMARK, hxRemark);
	}

	public List findAll() {
		log.debug("finding all HxApartmentLayout instances");
		try {
			String queryString = "from HxApartmentLayout";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public HxApartmentLayout merge(HxApartmentLayout detachedInstance) {
		log.debug("merging HxApartmentLayout instance");
		try {
			HxApartmentLayout result = (HxApartmentLayout) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(HxApartmentLayout instance) {
		log.debug("attaching dirty HxApartmentLayout instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(HxApartmentLayout instance) {
		log.debug("attaching clean HxApartmentLayout instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}