package gaubee.model;

/**
 * AbstractHxApartmentLayout entity provides the base persistence definition of
 * the HxApartmentLayout entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractHxApartmentLayout implements java.io.Serializable {

	// Fields

	private Integer hxId;
	private Integer hxLpId;
	private String hxName;
	private Double hxArea;
	private String hxType;
	private String hxPreview;
	private String hxStatus;
	private String hxRemark;

	// Constructors

	/** default constructor */
	public AbstractHxApartmentLayout() {
	}

	/** full constructor */
	public AbstractHxApartmentLayout(Integer hxLpId, String hxName,
			Double hxArea, String hxType, String hxPreview, String hxStatus,
			String hxRemark) {
		this.hxLpId = hxLpId;
		this.hxName = hxName;
		this.hxArea = hxArea;
		this.hxType = hxType;
		this.hxPreview = hxPreview;
		this.hxStatus = hxStatus;
		this.hxRemark = hxRemark;
	}

	// Property accessors

	public Integer getHxId() {
		return this.hxId;
	}

	public void setHxId(Integer hxId) {
		this.hxId = hxId;
	}

	public Integer getHxLpId() {
		return this.hxLpId;
	}

	public void setHxLpId(Integer hxLpId) {
		this.hxLpId = hxLpId;
	}

	public String getHxName() {
		return this.hxName;
	}

	public void setHxName(String hxName) {
		this.hxName = hxName;
	}

	public Double getHxArea() {
		return this.hxArea;
	}

	public void setHxArea(Double hxArea) {
		this.hxArea = hxArea;
	}

	public String getHxType() {
		return this.hxType;
	}

	public void setHxType(String hxType) {
		this.hxType = hxType;
	}

	public String getHxPreview() {
		return this.hxPreview;
	}

	public void setHxPreview(String hxPreview) {
		this.hxPreview = hxPreview;
	}

	public String getHxStatus() {
		return this.hxStatus;
	}

	public void setHxStatus(String hxStatus) {
		this.hxStatus = hxStatus;
	}

	public String getHxRemark() {
		return this.hxRemark;
	}

	public void setHxRemark(String hxRemark) {
		this.hxRemark = hxRemark;
	}

}