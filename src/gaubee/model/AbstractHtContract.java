package gaubee.model;

import java.sql.Timestamp;

/**
 * AbstractHtContract entity provides the base persistence definition of the
 * HtContract entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractHtContract implements java.io.Serializable {

	// Fields

	private String htId;
	private Integer htLfId;
	private Integer htKhId;
	private Integer htXsId;
	private Double htSoldPrice;
	private String htTermsOfPayment;
	private Timestamp htOrderTime;
	private Timestamp htSigningDate;
	private String htRemark;

	// Constructors

	/** default constructor */
	public AbstractHtContract() {
	}

	/** minimal constructor */
	public AbstractHtContract(String htId) {
		this.htId = htId;
	}

	/** full constructor */
	public AbstractHtContract(String htId, Integer htLfId, Integer htKhId,
			Integer htXsId, Double htSoldPrice, String htTermsOfPayment,
			Timestamp htOrderTime, Timestamp htSigningDate, String htRemark) {
		this.htId = htId;
		this.htLfId = htLfId;
		this.htKhId = htKhId;
		this.htXsId = htXsId;
		this.htSoldPrice = htSoldPrice;
		this.htTermsOfPayment = htTermsOfPayment;
		this.htOrderTime = htOrderTime;
		this.htSigningDate = htSigningDate;
		this.htRemark = htRemark;
	}

	// Property accessors

	public String getHtId() {
		return this.htId;
	}

	public void setHtId(String htId) {
		this.htId = htId;
	}

	public Integer getHtLfId() {
		return this.htLfId;
	}

	public void setHtLfId(Integer htLfId) {
		this.htLfId = htLfId;
	}

	public Integer getHtKhId() {
		return this.htKhId;
	}

	public void setHtKhId(Integer htKhId) {
		this.htKhId = htKhId;
	}

	public Integer getHtXsId() {
		return this.htXsId;
	}

	public void setHtXsId(Integer htXsId) {
		this.htXsId = htXsId;
	}

	public Double getHtSoldPrice() {
		return this.htSoldPrice;
	}

	public void setHtSoldPrice(Double htSoldPrice) {
		this.htSoldPrice = htSoldPrice;
	}

	public String getHtTermsOfPayment() {
		return this.htTermsOfPayment;
	}

	public void setHtTermsOfPayment(String htTermsOfPayment) {
		this.htTermsOfPayment = htTermsOfPayment;
	}

	public Timestamp getHtOrderTime() {
		return this.htOrderTime;
	}

	public void setHtOrderTime(Timestamp htOrderTime) {
		this.htOrderTime = htOrderTime;
	}

	public Timestamp getHtSigningDate() {
		return this.htSigningDate;
	}

	public void setHtSigningDate(Timestamp htSigningDate) {
		this.htSigningDate = htSigningDate;
	}

	public String getHtRemark() {
		return this.htRemark;
	}

	public void setHtRemark(String htRemark) {
		this.htRemark = htRemark;
	}

}