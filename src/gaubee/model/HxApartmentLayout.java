package gaubee.model;

/**
 * HxApartmentLayout entity. @author MyEclipse Persistence Tools
 */
public class HxApartmentLayout extends AbstractHxApartmentLayout implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public HxApartmentLayout() {
	}

	/** full constructor */
	public HxApartmentLayout(Integer hxLpId, String hxName, Double hxArea,
			String hxType, String hxPreview, String hxStatus, String hxRemark) {
		super(hxLpId, hxName, hxArea, hxType, hxPreview, hxStatus, hxRemark);
	}

}
