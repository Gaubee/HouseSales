package gaubee.model;

import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * KhCustomer entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see gaubee.model.KhCustomer
 * @author MyEclipse Persistence Tools
 */

public class KhCustomerDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(KhCustomerDAO.class);
	// property constants
	public static final String KH_UB_ID = "khUbId";
	public static final String KH_TYPE = "khType";
	public static final String KH_REMARK = "khRemark";

	public void save(KhCustomer transientInstance) {
		log.debug("saving KhCustomer instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(KhCustomer persistentInstance) {
		log.debug("deleting KhCustomer instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public KhCustomer findById(java.lang.Integer id) {
		log.debug("getting KhCustomer instance with id: " + id);
		try {
			KhCustomer instance = (KhCustomer) getSession().get(
					"gaubee.model.KhCustomer", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(KhCustomer instance) {
		log.debug("finding KhCustomer instance by example");
		try {
			List results = getSession().createCriteria(
					"gaubee.model.KhCustomer").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding KhCustomer instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from KhCustomer as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByKhUbId(Object khUbId) {
		return findByProperty(KH_UB_ID, khUbId);
	}

	public List findByKhType(Object khType) {
		return findByProperty(KH_TYPE, khType);
	}

	public List findByKhRemark(Object khRemark) {
		return findByProperty(KH_REMARK, khRemark);
	}

	public List findAll() {
		log.debug("finding all KhCustomer instances");
		try {
			String queryString = "from KhCustomer";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public KhCustomer merge(KhCustomer detachedInstance) {
		log.debug("merging KhCustomer instance");
		try {
			KhCustomer result = (KhCustomer) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(KhCustomer instance) {
		log.debug("attaching dirty KhCustomer instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(KhCustomer instance) {
		log.debug("attaching clean KhCustomer instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}