package gaubee.model;

/**
 * AbstractLpEstate entity provides the base persistence definition of the
 * LpEstate entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractLpEstate implements java.io.Serializable {

	// Fields

	private Integer lpId;
	private String lpName;
	private Double lpArea;
	private String lp3d;
	private String lpPreview;
	private String lpType;
	private String lpRemark;

	// Constructors

	/** default constructor */
	public AbstractLpEstate() {
	}

	/** full constructor */
	public AbstractLpEstate(String lpName, Double lpArea, String lp3d,
			String lpPreview, String lpType, String lpRemark) {
		this.lpName = lpName;
		this.lpArea = lpArea;
		this.lp3d = lp3d;
		this.lpPreview = lpPreview;
		this.lpType = lpType;
		this.lpRemark = lpRemark;
	}

	// Property accessors

	public Integer getLpId() {
		return this.lpId;
	}

	public void setLpId(Integer lpId) {
		this.lpId = lpId;
	}

	public String getLpName() {
		return this.lpName;
	}

	public void setLpName(String lpName) {
		this.lpName = lpName;
	}

	public Double getLpArea() {
		return this.lpArea;
	}

	public void setLpArea(Double lpArea) {
		this.lpArea = lpArea;
	}

	public String getLp3d() {
		return this.lp3d;
	}

	public void setLp3d(String lp3d) {
		this.lp3d = lp3d;
	}

	public String getLpPreview() {
		return this.lpPreview;
	}

	public void setLpPreview(String lpPreview) {
		this.lpPreview = lpPreview;
	}

	public String getLpType() {
		return this.lpType;
	}

	public void setLpType(String lpType) {
		this.lpType = lpType;
	}

	public String getLpRemark() {
		return this.lpRemark;
	}

	public void setLpRemark(String lpRemark) {
		this.lpRemark = lpRemark;
	}

}