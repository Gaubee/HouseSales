package gaubee.model;

/**
 * AbstractLfBuilding entity provides the base persistence definition of the
 * LfBuilding entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractLfBuilding implements java.io.Serializable {

	// Fields

	private Integer lfId;
	private Integer lfHxId;
	private String lfRoomNum;
	private String lfBuildingNum;
	private Double lfPrice;
	private String lfRemark;

	// Constructors

	/** default constructor */
	public AbstractLfBuilding() {
	}

	/** full constructor */
	public AbstractLfBuilding(Integer lfHxId, String lfRoomNum,
			String lfBuildingNum, Double lfPrice, String lfRemark) {
		this.lfHxId = lfHxId;
		this.lfRoomNum = lfRoomNum;
		this.lfBuildingNum = lfBuildingNum;
		this.lfPrice = lfPrice;
		this.lfRemark = lfRemark;
	}

	// Property accessors

	public Integer getLfId() {
		return this.lfId;
	}

	public void setLfId(Integer lfId) {
		this.lfId = lfId;
	}

	public Integer getLfHxId() {
		return this.lfHxId;
	}

	public void setLfHxId(Integer lfHxId) {
		this.lfHxId = lfHxId;
	}

	public String getLfRoomNum() {
		return this.lfRoomNum;
	}

	public void setLfRoomNum(String lfRoomNum) {
		this.lfRoomNum = lfRoomNum;
	}

	public String getLfBuildingNum() {
		return this.lfBuildingNum;
	}

	public void setLfBuildingNum(String lfBuildingNum) {
		this.lfBuildingNum = lfBuildingNum;
	}

	public Double getLfPrice() {
		return this.lfPrice;
	}

	public void setLfPrice(Double lfPrice) {
		this.lfPrice = lfPrice;
	}

	public String getLfRemark() {
		return this.lfRemark;
	}

	public void setLfRemark(String lfRemark) {
		this.lfRemark = lfRemark;
	}

}