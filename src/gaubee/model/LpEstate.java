package gaubee.model;

/**
 * LpEstate entity. @author MyEclipse Persistence Tools
 */
public class LpEstate extends AbstractLpEstate implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public LpEstate() {
	}

	/** full constructor */
	public LpEstate(String lpName, Double lpArea, String lp3d,
			String lpPreview, String lpType, String lpRemark) {
		super(lpName, lpArea, lp3d, lpPreview, lpType, lpRemark);
	}

}
