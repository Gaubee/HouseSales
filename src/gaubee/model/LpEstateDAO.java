package gaubee.model;

import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * LpEstate entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see gaubee.model.LpEstate
 * @author MyEclipse Persistence Tools
 */

public class LpEstateDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(LpEstateDAO.class);
	// property constants
	public static final String LP_NAME = "lpName";
	public static final String LP_AREA = "lpArea";
	public static final String LP3D = "lp3d";
	public static final String LP_PREVIEW = "lpPreview";
	public static final String LP_TYPE = "lpType";
	public static final String LP_REMARK = "lpRemark";

	public void save(LpEstate transientInstance) {
		log.debug("saving LpEstate instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(LpEstate persistentInstance) {
		log.debug("deleting LpEstate instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public LpEstate findById(java.lang.Integer id) {
		log.debug("getting LpEstate instance with id: " + id);
		try {
			LpEstate instance = (LpEstate) getSession().get(
					"gaubee.model.LpEstate", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(LpEstate instance) {
		log.debug("finding LpEstate instance by example");
		try {
			List results = getSession().createCriteria("gaubee.model.LpEstate")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding LpEstate instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from LpEstate as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByLpName(Object lpName) {
		return findByProperty(LP_NAME, lpName);
	}

	public List findByLpArea(Object lpArea) {
		return findByProperty(LP_AREA, lpArea);
	}

	public List findByLp3d(Object lp3d) {
		return findByProperty(LP3D, lp3d);
	}

	public List findByLpPreview(Object lpPreview) {
		return findByProperty(LP_PREVIEW, lpPreview);
	}

	public List findByLpType(Object lpType) {
		return findByProperty(LP_TYPE, lpType);
	}

	public List findByLpRemark(Object lpRemark) {
		return findByProperty(LP_REMARK, lpRemark);
	}

	public List findAll() {
		log.debug("finding all LpEstate instances");
		try {
			String queryString = "from LpEstate";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public LpEstate merge(LpEstate detachedInstance) {
		log.debug("merging LpEstate instance");
		try {
			LpEstate result = (LpEstate) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(LpEstate instance) {
		log.debug("attaching dirty LpEstate instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(LpEstate instance) {
		log.debug("attaching clean LpEstate instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}