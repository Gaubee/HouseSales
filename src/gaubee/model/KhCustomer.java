package gaubee.model;

/**
 * KhCustomer entity. @author MyEclipse Persistence Tools
 */
public class KhCustomer extends AbstractKhCustomer implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public KhCustomer() {
	}

	/** minimal constructor */
	public KhCustomer(Integer khId) {
		super(khId);
	}

	/** full constructor */
	public KhCustomer(Integer khId, String khUbId, String khType,
			String khRemark) {
		super(khId, khUbId, khType, khRemark);
	}

}
