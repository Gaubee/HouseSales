package gaubee.model;

/**
 * AbstractXsSalesStaff entity provides the base persistence definition of the
 * XsSalesStaff entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractXsSalesStaff implements java.io.Serializable {

	// Fields

	private Integer xsId;
	private String xsUbId;
	private String xsType;
	private String xsRemark;

	// Constructors

	/** default constructor */
	public AbstractXsSalesStaff() {
	}

	/** minimal constructor */
	public AbstractXsSalesStaff(Integer xsId) {
		this.xsId = xsId;
	}

	/** full constructor */
	public AbstractXsSalesStaff(Integer xsId, String xsUbId, String xsType,
			String xsRemark) {
		this.xsId = xsId;
		this.xsUbId = xsUbId;
		this.xsType = xsType;
		this.xsRemark = xsRemark;
	}

	// Property accessors

	public Integer getXsId() {
		return this.xsId;
	}

	public void setXsId(Integer xsId) {
		this.xsId = xsId;
	}

	public String getXsUbId() {
		return this.xsUbId;
	}

	public void setXsUbId(String xsUbId) {
		this.xsUbId = xsUbId;
	}

	public String getXsType() {
		return this.xsType;
	}

	public void setXsType(String xsType) {
		this.xsType = xsType;
	}

	public String getXsRemark() {
		return this.xsRemark;
	}

	public void setXsRemark(String xsRemark) {
		this.xsRemark = xsRemark;
	}

}