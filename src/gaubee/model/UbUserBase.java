package gaubee.model;

import java.sql.Timestamp;

/**
 * UbUserBase entity. @author MyEclipse Persistence Tools
 */
public class UbUserBase extends AbstractUbUserBase implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public UbUserBase() {
	}

	/** minimal constructor */
	public UbUserBase(String ubId) {
		super(ubId);
	}

	/** full constructor */
	public UbUserBase(String ubId, String ubName, String ubSex, String ubTel,
			Timestamp ubBirthDay, String ubAddress, String ubEmail,
			String ubSns, String ubRemark) {
		super(ubId, ubName, ubSex, ubTel, ubBirthDay, ubAddress, ubEmail,
				ubSns, ubRemark);
	}

}
