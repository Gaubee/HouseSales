package gaubee.model;

import java.sql.Timestamp;

/**
 * AbstractUbUserBase entity provides the base persistence definition of the
 * UbUserBase entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUbUserBase implements java.io.Serializable {

	// Fields

	private String ubId;
	private String ubName;
	private String ubSex;
	private String ubTel;
	private Timestamp ubBirthDay;
	private String ubAddress;
	private String ubEmail;
	private String ubSns;
	private String ubRemark;

	// Constructors

	/** default constructor */
	public AbstractUbUserBase() {
	}

	/** minimal constructor */
	public AbstractUbUserBase(String ubId) {
		this.ubId = ubId;
	}

	/** full constructor */
	public AbstractUbUserBase(String ubId, String ubName, String ubSex,
			String ubTel, Timestamp ubBirthDay, String ubAddress,
			String ubEmail, String ubSns, String ubRemark) {
		this.ubId = ubId;
		this.ubName = ubName;
		this.ubSex = ubSex;
		this.ubTel = ubTel;
		this.ubBirthDay = ubBirthDay;
		this.ubAddress = ubAddress;
		this.ubEmail = ubEmail;
		this.ubSns = ubSns;
		this.ubRemark = ubRemark;
	}

	// Property accessors

	public String getUbId() {
		return this.ubId;
	}

	public void setUbId(String ubId) {
		this.ubId = ubId;
	}

	public String getUbName() {
		return this.ubName;
	}

	public void setUbName(String ubName) {
		this.ubName = ubName;
	}

	public String getUbSex() {
		return this.ubSex;
	}

	public void setUbSex(String ubSex) {
		this.ubSex = ubSex;
	}

	public String getUbTel() {
		return this.ubTel;
	}

	public void setUbTel(String ubTel) {
		this.ubTel = ubTel;
	}

	public Timestamp getUbBirthDay() {
		return this.ubBirthDay;
	}

	public void setUbBirthDay(Timestamp ubBirthDay) {
		this.ubBirthDay = ubBirthDay;
	}

	public String getUbAddress() {
		return this.ubAddress;
	}

	public void setUbAddress(String ubAddress) {
		this.ubAddress = ubAddress;
	}

	public String getUbEmail() {
		return this.ubEmail;
	}

	public void setUbEmail(String ubEmail) {
		this.ubEmail = ubEmail;
	}

	public String getUbSns() {
		return this.ubSns;
	}

	public void setUbSns(String ubSns) {
		this.ubSns = ubSns;
	}

	public String getUbRemark() {
		return this.ubRemark;
	}

	public void setUbRemark(String ubRemark) {
		this.ubRemark = ubRemark;
	}

}