package gaubee.model;

import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * LfBuilding entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see gaubee.model.LfBuilding
 * @author MyEclipse Persistence Tools
 */

public class LfBuildingDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(LfBuildingDAO.class);
	// property constants
	public static final String LF_HX_ID = "lfHxId";
	public static final String LF_ROOM_NUM = "lfRoomNum";
	public static final String LF_BUILDING_NUM = "lfBuildingNum";
	public static final String LF_PRICE = "lfPrice";
	public static final String LF_REMARK = "lfRemark";

	public void save(LfBuilding transientInstance) {
		log.debug("saving LfBuilding instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(LfBuilding persistentInstance) {
		log.debug("deleting LfBuilding instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public LfBuilding findById(java.lang.Integer id) {
		log.debug("getting LfBuilding instance with id: " + id);
		try {
			LfBuilding instance = (LfBuilding) getSession().get(
					"gaubee.model.LfBuilding", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(LfBuilding instance) {
		log.debug("finding LfBuilding instance by example");
		try {
			List results = getSession().createCriteria(
					"gaubee.model.LfBuilding").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding LfBuilding instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from LfBuilding as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByLfHxId(Object lfHxId) {
		return findByProperty(LF_HX_ID, lfHxId);
	}

	public List findByLfRoomNum(Object lfRoomNum) {
		return findByProperty(LF_ROOM_NUM, lfRoomNum);
	}

	public List findByLfBuildingNum(Object lfBuildingNum) {
		return findByProperty(LF_BUILDING_NUM, lfBuildingNum);
	}

	public List findByLfPrice(Object lfPrice) {
		return findByProperty(LF_PRICE, lfPrice);
	}

	public List findByLfRemark(Object lfRemark) {
		return findByProperty(LF_REMARK, lfRemark);
	}

	public List findAll() {
		log.debug("finding all LfBuilding instances");
		try {
			String queryString = "from LfBuilding";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public LfBuilding merge(LfBuilding detachedInstance) {
		log.debug("merging LfBuilding instance");
		try {
			LfBuilding result = (LfBuilding) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(LfBuilding instance) {
		log.debug("attaching dirty LfBuilding instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(LfBuilding instance) {
		log.debug("attaching clean LfBuilding instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}