package gaubee.model;

/**
 * AbstractKhCustomer entity provides the base persistence definition of the
 * KhCustomer entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractKhCustomer implements java.io.Serializable {

	// Fields

	private Integer khId;
	private String khUbId;
	private String khType;
	private String khRemark;

	// Constructors

	/** default constructor */
	public AbstractKhCustomer() {
	}

	/** minimal constructor */
	public AbstractKhCustomer(Integer khId) {
		this.khId = khId;
	}

	/** full constructor */
	public AbstractKhCustomer(Integer khId, String khUbId, String khType,
			String khRemark) {
		this.khId = khId;
		this.khUbId = khUbId;
		this.khType = khType;
		this.khRemark = khRemark;
	}

	// Property accessors

	public Integer getKhId() {
		return this.khId;
	}

	public void setKhId(Integer khId) {
		this.khId = khId;
	}

	public String getKhUbId() {
		return this.khUbId;
	}

	public void setKhUbId(String khUbId) {
		this.khUbId = khUbId;
	}

	public String getKhType() {
		return this.khType;
	}

	public void setKhType(String khType) {
		this.khType = khType;
	}

	public String getKhRemark() {
		return this.khRemark;
	}

	public void setKhRemark(String khRemark) {
		this.khRemark = khRemark;
	}

}