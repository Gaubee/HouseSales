package gaubee.model;

/**
 * XsSalesStaff entity. @author MyEclipse Persistence Tools
 */
public class XsSalesStaff extends AbstractXsSalesStaff implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public XsSalesStaff() {
	}

	/** minimal constructor */
	public XsSalesStaff(Integer xsId) {
		super(xsId);
	}

	/** full constructor */
	public XsSalesStaff(Integer xsId, String xsUbId, String xsType,
			String xsRemark) {
		super(xsId, xsUbId, xsType, xsRemark);
	}

}
