package gaubee.model;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * HtContract entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see gaubee.model.HtContract
 * @author MyEclipse Persistence Tools
 */

public class HtContractDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(HtContractDAO.class);
	// property constants
	public static final String HT_LF_ID = "htLfId";
	public static final String HT_KH_ID = "htKhId";
	public static final String HT_XS_ID = "htXsId";
	public static final String HT_SOLD_PRICE = "htSoldPrice";
	public static final String HT_TERMS_OF_PAYMENT = "htTermsOfPayment";
	public static final String HT_REMARK = "htRemark";

	public void save(HtContract transientInstance) {
		log.debug("saving HtContract instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(HtContract persistentInstance) {
		log.debug("deleting HtContract instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public HtContract findById(java.lang.String id) {
		log.debug("getting HtContract instance with id: " + id);
		try {
			HtContract instance = (HtContract) getSession().get(
					"gaubee.model.HtContract", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(HtContract instance) {
		log.debug("finding HtContract instance by example");
		try {
			List results = getSession().createCriteria(
					"gaubee.model.HtContract").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding HtContract instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from HtContract as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByHtLfId(Object htLfId) {
		return findByProperty(HT_LF_ID, htLfId);
	}

	public List findByHtKhId(Object htKhId) {
		return findByProperty(HT_KH_ID, htKhId);
	}

	public List findByHtXsId(Object htXsId) {
		return findByProperty(HT_XS_ID, htXsId);
	}

	public List findByHtSoldPrice(Object htSoldPrice) {
		return findByProperty(HT_SOLD_PRICE, htSoldPrice);
	}

	public List findByHtTermsOfPayment(Object htTermsOfPayment) {
		return findByProperty(HT_TERMS_OF_PAYMENT, htTermsOfPayment);
	}

	public List findByHtRemark(Object htRemark) {
		return findByProperty(HT_REMARK, htRemark);
	}

	public List findAll() {
		log.debug("finding all HtContract instances");
		try {
			String queryString = "from HtContract";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public HtContract merge(HtContract detachedInstance) {
		log.debug("merging HtContract instance");
		try {
			HtContract result = (HtContract) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(HtContract instance) {
		log.debug("attaching dirty HtContract instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(HtContract instance) {
		log.debug("attaching clean HtContract instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}