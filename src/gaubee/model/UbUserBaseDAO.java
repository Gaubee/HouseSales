package gaubee.model;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * UbUserBase entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see gaubee.model.UbUserBase
 * @author MyEclipse Persistence Tools
 */

public class UbUserBaseDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UbUserBaseDAO.class);
	// property constants
	public static final String UB_NAME = "ubName";
	public static final String UB_SEX = "ubSex";
	public static final String UB_TEL = "ubTel";
	public static final String UB_ADDRESS = "ubAddress";
	public static final String UB_EMAIL = "ubEmail";
	public static final String UB_SNS = "ubSns";
	public static final String UB_REMARK = "ubRemark";

	public void save(UbUserBase transientInstance) {
		log.debug("saving UbUserBase instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(UbUserBase persistentInstance) {
		log.debug("deleting UbUserBase instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UbUserBase findById(java.lang.String id) {
		log.debug("getting UbUserBase instance with id: " + id);
		try {
			UbUserBase instance = (UbUserBase) getSession().get(
					"gaubee.model.UbUserBase", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UbUserBase instance) {
		log.debug("finding UbUserBase instance by example");
		try {
			List results = getSession().createCriteria(
					"gaubee.model.UbUserBase").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding UbUserBase instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from UbUserBase as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByUbName(Object ubName) {
		return findByProperty(UB_NAME, ubName);
	}

	public List findByUbSex(Object ubSex) {
		return findByProperty(UB_SEX, ubSex);
	}

	public List findByUbTel(Object ubTel) {
		return findByProperty(UB_TEL, ubTel);
	}

	public List findByUbAddress(Object ubAddress) {
		return findByProperty(UB_ADDRESS, ubAddress);
	}

	public List findByUbEmail(Object ubEmail) {
		return findByProperty(UB_EMAIL, ubEmail);
	}

	public List findByUbSns(Object ubSns) {
		return findByProperty(UB_SNS, ubSns);
	}

	public List findByUbRemark(Object ubRemark) {
		return findByProperty(UB_REMARK, ubRemark);
	}

	public List findAll() {
		log.debug("finding all UbUserBase instances");
		try {
			String queryString = "from UbUserBase";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public UbUserBase merge(UbUserBase detachedInstance) {
		log.debug("merging UbUserBase instance");
		try {
			UbUserBase result = (UbUserBase) getSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(UbUserBase instance) {
		log.debug("attaching dirty UbUserBase instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UbUserBase instance) {
		log.debug("attaching clean UbUserBase instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}