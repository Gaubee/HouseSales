package gaubee.model;

import java.sql.Timestamp;

/**
 * HtContract entity. @author MyEclipse Persistence Tools
 */
public class HtContract extends AbstractHtContract implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public HtContract() {
	}

	/** minimal constructor */
	public HtContract(String htId) {
		super(htId);
	}

	/** full constructor */
	public HtContract(String htId, Integer htLfId, Integer htKhId,
			Integer htXsId, Double htSoldPrice, String htTermsOfPayment,
			Timestamp htOrderTime, Timestamp htSigningDate, String htRemark) {
		super(htId, htLfId, htKhId, htXsId, htSoldPrice, htTermsOfPayment,
				htOrderTime, htSigningDate, htRemark);
	}

}
